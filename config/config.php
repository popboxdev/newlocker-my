<?php
$data = [];
if (env('APP_ENV') == "production") {
    $data = [
        'ip_access' => ['161.202.172.184', '119.81.16.117'],
        'chiper_text' => "p0pb0x.Asia",
        'chain' => '1',
        'ecash_path' => 'https://mandiriecash.com/ecommgateway/',
        'ecash_mid' => '49815000265',
        'ecash_tokenIPG' => 'DBAA02882F34EC5E40BC0BD5FCB47343',
        'ecash_server_merchant' => '119.81.16.117',
        'shop_host' => 'https://shop.popbox.asia/',
        'api_host' => 'https://internalapi.popbox.asia/',
        'redirect_popsend' => 'https://popsend.popbox.asia/topup/process',
        'redirect_popshop' => 'https://shop.popbox.asia/payment/dokutransaction',
        'domain_production' => 'https://shop.popbox.asia',
        'popsend_url' => 'https://popsend.popbox.asia',
        'img_location' => 'https://shop.popbox.asia/assets/images',
        'email_sender1' => 'lidya@popbox.asia',
        'email_sender2' => 'greta@popbox.asia',
        'rollbar' => [
            'access_token' => '9258a514307642699fa06018168d4914',
            'level' => 'error',
        ],
        'jne' => [
            'url' => 'http://api.jne.co.id:8889/tracing/apitest/pricedev',
            'origin' => 'http://api.jne.co.id:8889/tracing/apitest/dest/key',
            'dest' => 'http://api.jne.co.id:8889/tracing/apitest/dest/key/',
            'username' => 'TESTAPI',
            'api_key' => '25c898a9faea1a100859ecd9ef674548',
        ],
        "domain_email_merchant_reg" => "https://www.popbox.asia",
        "email_merchant_reg" => "merchant@popbox.asia",
        "email_order_shop_bcc" => "dyah@popbox.com",
        'molpay' => array(
            'ip_ipay' => array('127.0.0.1','111.67.33.90'),
            'url_submit' => 'https://www.onlinepayment.com.my/MOLPay/pay/popbox_Dev/index.php',
            'merchant_id' => 'popbox_Dev',
            'verify_key' => '433363bc15f8338a0562a13883345a62',
            'return_url' => 'https://internalapi.popbox.asia/payment/molpay/response'
        ),
    ];
} else if (env('APP_ENV') == "staging") {
    $data = [
        'ip_access' => ['161.202.172.184', '119.81.16.117'],
        'chiper_text' => "p0pb0x.Asia",
        'chain' => 'NA',
        'ecash_path' => 'https://sandbox.mandiri-ecash.com/ecommgateway/',
        'ecash_mid' => 'popbox',
        'ecash_tokenIPG' => '082EE702C274E8F5319E37B61E123EDC',
        'ecash_server_merchant' => '161.202.172.184',
        'shop_host' => 'http://shopdev.popbox.asia/',
        'api_host' => 'http://api-dev.popbox.asia/',
        'redirect_popsend' => 'http://popsendev.popbox.asia/topup/process',
        'redirect_popshop' => 'http://shopdev.popbox.asia/payment/dokutransaction',
        'domain_production' => 'http://shopdev.popbox.asia',
        'popsend_url' => 'http://popsendev.popbox.asia',
        'img_location' => 'http://dimo.popbox.asia/assets/images',
        'email_sender1' => 'christian@popbox.asia',
        'email_sender2' => 'dyah@popbox.asia',
        'rollbar' => [
            'access_token' => '9258a514307642699fa06018168d4914',
            'level' => 'error'
        ],
        'jne' => [
            'url' => 'http://api.jne.co.id:8889/tracing/apitest/pricedev',
            'origin' => 'http://api.jne.co.id:8889/tracing/apitest/dest/key',
            'dest' => 'http://api.jne.co.id:8889/tracing/apitest/dest/key/',
            'username' => 'TESTAPI',
            'api_key' => '25c898a9faea1a100859ecd9ef674548',
        ],
        "domain_email_merchant_reg" => "http://beta.popbox.asia",
        "email_merchant_reg" => "merchant@popbox.asia",
        "email_order_shop_bcc" => "dyah@popbox.com",
        'molpay' => array(
            'ip_ipay' => array('127.0.0.1','111.67.33.90'),
            'url_submit' => 'https://www.onlinepayment.com.my/MOLPay/pay/test7776/',
            'merchant_id' => 'test7776',
            'verify_key' => 'f5236a94bbf8c2d92b544e6ab94d3aed',
            'return_url' => 'http://api-dev.popbox.asia/payment/molpay/response'
        ),
    ];
} else {
    $data = [
        'ip_access' => ['161.202.172.184', '119.81.16.117'],
        'chiper_text' => "p0pb0x.Asia",
        'chain' => 'NA',
        'ecash_path' => 'https://sandbox.mandiri-ecash.com/ecommgateway/',
        'ecash_mid' => 'popbox',
        'ecash_tokenIPG' => '082EE702C274E8F5319E37B61E123EDC',
        'ecash_server_merchant' => '161.202.172.184',
        'shop_host' => 'http://popshopweb.dev/',
        'api_host' => 'http://apiv2.dev/',
        'redirect_popsend' => 'http://popsend.dev/topup/process',
        'redirect_popshop' => 'http://popshopweb.dev/payment/dokutransaction',
        'domain_production' => 'http://popshopweb.dev',
        'popsend_url' => 'http://popsend.dev',
        'img_location' => 'http://dimo.popbox.asia/assets/images',
        'email_sender1' => 'nungky@popbox.asia',
        'email_sender2' => 'nungky@popbox.asia',
        'rollbar' => [
            'access_token' => '9258a514307642699fa06018168d4914',
            'level' => 'error',
        ],
        'jne' => [
            'url' => 'http://api.jne.co.id:8889/tracing/apitest/pricedev',
            'origin' => 'http://api.jne.co.id:8889/tracing/apitest/dest/key',
            'dest' => 'http://api.jne.co.id:8889/tracing/apitest/dest/key/',
            'username' => 'TESTAPI',
            'api_key' => '25c898a9faea1a100859ecd9ef674548',
        ],
        "domain_email_merchant_reg" => "http://popbox.dev",
        "email_merchant_reg" => "lee.nungky@gmail.com",
        "email_order_shop_bcc" => "lee.nungky@gmail.com",
        'molpay' => array(
            'ip_ipay' => array('127.0.0.1','111.67.33.90'),
            'url_submit' => 'https://www.onlinepayment.com.my/MOLPay/pay/test7776/',
            'merchant_id' => 'test7776',
            'verify_key' => 'f5236a94bbf8c2d92b544e6ab94d3aed',
            'return_url' => 'http://apiv2.dev/payment/molpay/response'
        ),
    ];
}
return $data;