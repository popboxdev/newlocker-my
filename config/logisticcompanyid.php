<?php 

if (env('APP_ENV') == "development") {
    $config = [
        "lazada_express" => "402880825c2ddbf5015c42c61eeb2647",
        "lazada_indonesia" => "810385794001569996692BI47izvnuFx",
        "pos_malaysia" => "800640693001567595171qEYKp7s4CTR",
        "pop_express" => "4370706542001568969462X32LXymGST",
        "meikarta" => "2360793331001576221380iJLUTDRpSa",
        "dhl_malaysia" => "8100396495001590558407U0kjOpMtEy",
        "digital_condo" => "54406141710015929798062dKQ3QIldj"
    ];
} else {
    $config = [
        "lazada_express" => "402880825c2ddbf5015c42c61eeb2647",
        "lazada_indonesia" => "2c9180c151a8c1a70151ccfc1cc109ca",
        "pos_malaysia" => "402880825ea763b8015fd71178de212b",
        "pop_express" => "161e5ed1140f11e5bdbd0242ac110001",
        "meikarta" => "84803089410015762204595QPERPdCLk",
        "dhl_malaysia" => "7250121760001575251914IuJgPNUisw",
        "digital_condo" => "54406141710015929798062dKQ3QIldj"
    ];
}
return $config;