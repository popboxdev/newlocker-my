<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTbWaLogNotificationMalaysia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasTable('tb_wa_log_malaysia'))) {
            Schema::create('tb_wa_log_malaysia', function (Blueprint $table) {
                $table->increments('id');
                $table->string('parcel_id')->nullable();
                $table->string('parcel_number')->nullable();
                $table->string('phone_number')->nullable();
                $table->string('log_request')->nullable();
                $table->string('log_response')->nullable();
                $table->timestamps();
           });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
