<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p> Lazada order list  </p>
		<p> Date : {{ $tgl_now }} {{ $jam }}:00</p>
		<p> Location : Mangga Dua Mall </p>
		<p>
		<table style="border-spacing: 0;border: 1px solid black;">	
			<tr style="background:#f9f9f9;">
				<td style="border-spacing: 0;border: 1px solid black;">No</td>
				<td style="border-spacing: 0;border: 1px solid black;">Login ID</td>
				<td style="border-spacing: 0;border: 1px solid black;">Barcode</td>
				<td style="border-spacing: 0;border: 1px solid black;">Seller</td>
				<td style="border-spacing: 0;border: 1px solid black;">PIN Code</td>
				<td style="border-spacing: 0;border: 1px solid black;">Store Time</td>
				<td style="border-spacing: 0;border: 1px solid black;">Remarks</td>	
			</tr>
			 <?php $i = 0?> 
			@foreach ($res as $rs)
			<tr>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ ++$i }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rs->name }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rs->barcode }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rs->seller_name }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rs->validatecode }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ date("d/m/Y H:i:s", strtotime($rs->storetime))  }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rs->remarks }}</td>
			</tr>
			@endforeach
		</table>		
	</body>
</html>