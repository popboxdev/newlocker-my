<html>
<body>
    Hai Admin,<br/><br/>

    Ada Pesanan PopShop baru dengan no order {{$order["invoice_id"]}} yang harus segera diproses<br/><br/>

    Email : {{$order["email"]}}<br/>
    Phone : {{$order["phone"]}}<br/>
    Destination : {{$order["address"]}}<br/>
    Store name : PopShop<br/>
    Payment type : {{$pay_type}}<br/>
    Total Amount : Rp. {{$order["amount"]}}<br/><br/>

    <table style="border-spacing: 0px">
        <tr>
            <td style="border: 1px solid black">SKU</td>
            <td style="border: 1px solid black">Product Name</td>
            <td style="border: 1px solid black">Quantity</td>
            <td style="border: 1px solid black">Price</td>
            <td style="border: 1px solid black">Discount</td>
        </tr>
        @foreach ($detail as $key => $value)
            <tr>
                <td style="border: 1px solid black">{{$value["sku"]}}</td>
                <td style="border: 1px solid black">{{$value["product_name"]}}</td>
                <td style="border: 1px solid black">{{$value["quantity"]}}</td>
                <td style="border: 1px solid black">{{$value["price"]}}</td>
                <td style="border: 1px solid black">{{$value["discount"]}}</td>
            </tr>
        @endforeach
    </table>
</body>
</html>
