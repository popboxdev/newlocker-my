<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <title>Reset Password</title>
</head>
<body style="font-family:arial">
<table  cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td><img src="{{URL::asset('/img/email/logo-popbox.png')}}" width="100px"/></td>
			<td style="width: 56%"></td>
			<td>&nbsp;</td>
		</tr>
		<tr><td colspan="3" style="border-top: 1px solid #000;"></td></tr>
		<tr><td colspan="3">
			<p>Hai {{ $name }}, </p>
			<p> Anda telah melakukan permintaan untuk perubahan password pada akun {{ $phone }},</p>
			<p> berikut adalah password baru anda <p>
			<p style="font-size:24px"> {{ $mem_pwd }}  </p>
			<p> Setelah melakukan Log in pada App PopBox ataupun web, harap ubah password Anda segera.
			<ul>
				<li> Buka halaman My PopBox, pilih Change Password pada Apps (Android dan iOS)
				<li> Klik username anda di pojok kanan atas, lalu pilih Reset Password 
			</ul>
			</p>

			</td>
		</tr>
		<tr><td colspan="3">
			<p>Jika anda mengalami kesulitan harap hubungi info@popbox.asia atau 021 - 29022537 	</p>	
		</tr>
		<tr><td colspan="3"><br><br>
			Salam Hangat,<br><br>
			PopBox Team <br>
			www.popbox.asia	
		</tr>
		<tr><td>
			&nbsp;
			</td>
		</tr>	
		<tr><td>
			&nbsp;
			</td>
		</tr>		
	</tbody> 
</table>				
</body>
</html>