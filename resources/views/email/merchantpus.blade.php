<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Hai Admin, </p>
		<p> Ada Merchant Pickup Request Baru dari {{ $merchant_name }} pada {{ $tglsubmit }} yang harus segera diproses  </p>
		<p>
		<table style="border-spacing: 0;border: 1px solid black;">	
			<tr style="background:#f9f9f9;">
				<td style="border-spacing: 0;border: 1px solid black;">Order No</td>
				<td style="border-spacing: 0;border: 1px solid black;">Pickup Location</td>
				<td style="border-spacing: 0;border: 1px solid black;">Popbox Location</td>
				<td style="border-spacing: 0;border: 1px solid black;">Seller Phone</td>
				<td style="border-spacing: 0;border: 1px solid black;">Weight</td>
				<td style="border-spacing: 0;border: 1px solid black;">Customer Phone</td>
				<td style="border-spacing: 0;border: 1px solid black;">Customer Email</td>	
			</tr>
			@foreach ($rsmpus as $rsmpu)
			<tr>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rsmpu->order_number }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rsmpu->pickup_location }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rsmpu->popbox_location }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rsmpu->seller_phone }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rsmpu->weight }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rsmpu->customer_phone }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $rsmpu->customer_email }}</td>
			</tr>
			@endforeach
		</table>		
	</body>
</html>