<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8;image/jpeg">

    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <title>PopShop Invoice</title>
</head>
<body style="font-family:arial">	
	<table cellspacing="0" cellpadding="0" width="100%">
		<tbody>					
			<tr>
				<td>					
					<div style="float: left"><img src="{{URL::asset('img/email/logo-popbox.png')}}"/></div>
					<div style="float: right;margin: 23px;"><a href="www.popbox.asia" style="text-decoration: none;font-size: 30px;color: #000;">www.popbox.asia</a></div>
				</td>
			</tr>
			<tr>
				
			<td style="text-align: center;color: #fff;background: #4ac0c1;">
					<div style="font-size: 25px;margin: 35px;">
						Hi {{$member_name}}
					</div>
					<div style="line-height: 26px;font-size: 19px;margin: 32px 0px;">
						Terima kasih!<br/>
						Order {{$invoice_id}} sudah kami terima!
					</div>
					<div style="background: #FFF;color: #000;width: 70%;margin: 0 auto;padding: 0px 1%;border-top-right-radius: 2em;border-top-left-radius: 2em;padding-top: 53px;">					
						<table cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td style="text-align: left;font-size: 25px;">
									<div style="margin-left:17%">
									Product
									</div>
								</td>
								<td style="text-align: left;">&nbsp;</td>
								<td style="text-align: left;font-size: 25px;">
									<div style="margin-left:17%">
										Total
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div style="border-top:1px solid #000;width: 100%"></div>
								</td>
							</tr>
							<?php 
								$total = 0; 
								$kirim = 0; 
							?>
							@foreach ($detail['detail'] as $value)
								<?php $total = $total + ($value->quantity*$value->price) ; ?>
								<tr>
									<td style="width: 22%">
										<img src="{{$value->image}}" style="width: 60%;padding: 17px;" />
									</td>
									<td style="text-align: left;vertical-align: top;">
										<div style="margin: 26px 0px;">
											{{$value->name}}<br/>
											Jumlah : {{number_format($value->quantity,0,',','.')}}
										</div>
									</td>
									<td style="vertical-align: top;">						
										<div style="margin: 26px 0px;">			
											Rp {{number_format($value->quantity*$value->price,0,',','.')}}
										</div>
									</td>
								</tr>	
								<tr>
									<td colspan="3">
										<div style="border-top: 1px solid #e1e1e1;"></div>
									</td>									
								</tr>							
							@endforeach							
							<tr>
									<td colspan="3">
										<div style="height: 26px">
											&nbsp;
										</div>
									</td>									
							</tr>	
							<tr>
								<td colspan="2" style="text-align:right">SubTotal :</td>
								<td>Rp {{number_format($total,0,',','.')}}</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align:right">Ongkos Kirim :</td>
								<td>Rp {{$kirim}}</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align:right"><b>Total :</b></td>
								<td><b>Rp {{number_format($total+$kirim,0,',','.')}}</b></td>
							</tr>
							<tr>
								<td colspan="3" style="text-align:center;">
									<div style="margin-top: 11%;">
										<b>Silahkan lakukan pembayaran 1x24 jam, <br>
										agar pesanan kamu dapat segera kami proses</b>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3" style="text-align:center;">
									<div style="border-top: 1px solid #000;width: 60%;margin: 0 auto;margin-top: 26px;    margin-bottom: 26px;">
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3" style="text-align:center;">
									<div>
										<img src="{{URL::asset('/img/email/logo-mandiri.png')}}"><img src="{{URL::asset('/img/email/logo-bca.png')}}">
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3" style="text-align:center;">
									<span style="margin-right: 74px;">
										165.000.91.2222.8										
									</span>
									<span>
										5260.35.8822
									</span>
								</td>
							</tr>

							<tr>
								<td colspan="3" style="text-align:center;">
									<div style="margin: 23px 0px;">
										a/n PT. PopBox Asia Services
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3" style="text-align:center;">
									<div style="border-top: 1px solid #000;width: 60%;margin: 0 auto;margin-bottom: 26px;">
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3" style="margin: 26px 0px;">
									<div>
										Setelah melakukan pembayaran harap segera lakukan konfirmasi pembayaran.<br/>
										Mohon tunggu paling lama 1 hari untuk mendapatkan informasi<br/>
										proses verifikasi pembayaran anda
									</div>
								</td>
							</tr>	
							<tr>
								<td colspan="3" style="margin: 26px 0px;">
									<a href="{{config('config.domain_production')}}/paybyqr/payment/comfirmation" style="text-decoration: none;">
										<div style="background: #ea1c24;color: #FFF;width: 40%;margin: 0 auto;margin-top: 20px;padding: 10px 0px;border-radius: 10px;margin-bottom: 27px;">
											Konfirmasi pembayaran
										</div>
									</a>
								</td>
							</tr>	
						</table>
					</div>
					<div style=";color: #000;width: 62%;margin: 0 auto;">
						<table cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td colspan="3">
									<div style="background: #289393;height: 6px;width: 100%">
									</div>								
								</td>							
							</tr>
							<tr>
								<td colspan="3">
									<div style="background: #4ac0c1;width: 100%;color: #FFF;padding: 20px;line-height: 30px;">
										Apabila ada pertanyaan, kami siap membantu anda<br/>
										Silahkan hubungi kami melalui<br/>
										<span style="text-decoration:none;color: #000">info@popbox.asia</span> atau <span style="font-size: 23px;text-decoration:none;color: #000">021 - 29022537</span>
									</div>								
								</td>							
							</tr>

						</table>
					</div>
				</td>				
			</tr>		
			<tr style="background: #5d5d5e;">
							<td colspan="3" style="text-align: center;height: 50px">
								<a href="https://www.facebook.com/pboxasia"><img src="{{URL::asset('/img/email/logo-fb1.png')}}" style="margin-right: 10%"></a>
								<a href="https://instagram.com/popbox_asia"><img src="{{URL::asset('/img/email/logo-ins.png')}}" style="margin-right: 10%"></a>
								<a href="https://twitter.com/popbox_asia"><img src="{{URL::asset('/img/email/logo-tw.png')}}"></a>
							</td>
						</tr>
		</tbody> 
	</table>		
</body>
</html>