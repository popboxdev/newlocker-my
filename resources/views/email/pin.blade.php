Pelanggan terhormat,<br>
<br>
Order {{ $storename }} Anda ({{ $barcode }}) sudah tiba di E-locker @ {{ $locker_name }}. Mohon untuk segera diambil sebelum {{ $overduetime }}. <br>
<br>
Detail Lokasi : <br>
<br>
{{ $locker_name }} ({{ $address_2 }})<br>
{{ $address }}<br>
{{ $operational_hours }} <br>
<br>
Bagaimana Cara Mengambil Parcel Anda?<br>
1. Pilih Opsi Bahasa,<br>
2. Pilih Opsi Mengambil Barang, <br>
3. Masukkan Kode 6 digit PIN yang Anda dapatkan dari SMS atau Email ini, <br>
4. Jangan Lupa Menutup Kembali Pintu Locker. Enjoy ! <br>
<br>
<font size="4">PIN: <b>{{ $validatecode }}</b></font> <br>
<br>
Mohon Untuk Menjaga Kerahasiaan PIN Anda. Segala Kerugian atau Kehilangan Atas Kelalaian Menjaga Kerahasiaan PIN Menjadi Tanggung Jawab Pribadi. <br>
<br>
Terima Kasih Telah Menggunakan E-locker. Informasi Lengkap Lokasi E-locker/Popbox di www.popbox.asia/locations.<br>
<br>

@if ($prefix == 'MAIS')
Untuk Informasi Customer Care Hubungi 1500038 atau Kunjungi www.mataharimall.com <br>
@endif
<br>  

Salam Hangat, <br>
<br>
PopBox Asia <br>
Grand Slipi Tower Unit 18J <br>
Jl. Letjen S.Parman Kav 22-24 <br>
Jakarta Barat 11480 <br>
Tlp.021-29022537/38 <br>
www.popbox.asia <br>