<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Hi Admin, </p>

		@if (!empty($partner_name))
			<p> New Merchant Pickup Request from {{ $partner_name }} on {{ $tglsubmit }}   </p>
		@else
			<p> New Merchant Pickup Request from {{ $merchant_name }} on {{ $tglsubmit }}   </p>
		@endif
		<p>
			Order No : {{ $order_number }} <br />
			Merchant Name : {{ $merchant_name }}<br />
			Pickup Location : {{ $pickup_location }}<br />
			@if (!empty($seller_phone))
				Seller Phone : {{ $seller_phone }} <br/>
			@endif
			
			@if (!empty($weight))
				Weight : {{ $weight  }} <br/>
			@endif
			
			@if (!empty($payment_type))
				Payment Type : {{ $payment_type  }} <br/>
			@endif

			Popbox Location : {{ $popbox_location }} <br/>
			Customer phone :  {{ $customer_phone }} <br/>
			Customer email :   {{ $customer_email }} <br/>
		</p>		
	</body>
</html>