<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Hai Admin, </p>
		<p>Ada Pesanan PopShop baru dengan no order  {{ $invoice_id }} yang harus segera diproses  </p>
		
		Email :  {{ $email }} <br>
		Phone :  {{ $phone }}<br>
		Destination :  {{ $address }}  <br>
		Store name : {{ $store_id }} <br>
		Total Amount : {{ $amount }}
		<br><br>

		<table style="border-spacing: 0;border: 1px solid black;">	
			<tr style="background:#f9f9f9;">
				<td style="border-spacing: 0;border: 1px solid black;">SKU</td>
				<td style="border-spacing: 0;border: 1px solid black;">Product Name</td>
				<td style="border-spacing: 0;border: 1px solid black;">Quantity</td>
				<td style="border-spacing: 0;border: 1px solid black;">Price</td>
			</tr>
			
			@foreach ($products as $product)
			<tr>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $product->sku }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $product->name }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $product->quantity }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $product->price }}</td>
			</tr>
			@endforeach
		</table>		
	</body>
</html>