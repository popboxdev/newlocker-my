<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
	<head>
		<title></title>

		<link href="{{URL::asset('css/bootstrap.css')}}" rel="stylesheet">		
		<link href="{{URL::asset('css/bootstrap-theme.css')}}" rel="stylesheet">
		<link href="{{URL::asset('css/payment.css')}}" rel="stylesheet">
		
		<script src="{{URL::asset('js/jquery.min.js')}}"></script>
		<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
	</head>
	<body>
		<div id="head">			
		</div>
		<div id="content">		
			<div class="container container-fluid">        	
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 failed-img">
					<center>
						<img src="{{URL::asset('img/payment/succeed.png')}}" width="96" height="96"><br/>
						<div class="word-failed">Thank you</div>
						<br>
						<div class="word-desc">Your payment is completed, please check your balance<br/>
								Press back button to close this page
						</center>
						</div>
						<!-- <div class="btn-close">Close</div> -->
					</div>
				</div>
			</div>
			
		</div>
		<div id="footer"></div>
	</body>
</html>
<script>
	$(document).ready(function(){
		$(".btn-close").click(function(){
			 
		})
	})
</script>
