<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
	<head>
		<title></title>

		<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">		
		<link href="{{URL::asset('css/bootstrap-theme.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('css/payment.css')}}" rel="stylesheet">
		
		<script src="{{URL::asset('js/jquery.min.js')}}"></script>
		<script src="{{URL::asset('js/bootstrap.js')}}"></script>
	</head>	
</haed>
<BODY>
	<img src="{{URL::asset('img/payment/loader.gif')}}">
	<FORM method="post" name="ePayment" id="ipay88frm" action="https://www.mobile88.com/ePayment/entry.asp">
	    <INPUT type="hidden" name="MerchantCode" value="M04500">
	    <INPUT type="hidden" name="PaymentId" value="2">
	    <INPUT type="hidden" name="RefNo" value="<?php echo $refno; ?>">
	    <INPUT type="hidden" name="Amount" value="1.00">
	    <INPUT type="hidden" name="Currency" value="MYR">
	    <INPUT type="hidden" name="ProdDesc" value="Top Up Balance">
	    <INPUT type="hidden" name="UserName" value="<?php echo $name; ?>"></td>
	    <INPUT type="hidden" name="UserEmail" value="<?php echo $email; ?>">
	    <INPUT type="hidden" name="UserContact" value="<?php echo $phone; ?>">
	    <INPUT type="hidden" name="Remark" value="">
	    <INPUT type="hidden" name="Lang" value="UTF-8">
	    <INPUT type="hidden" name="Signature" value="">
	    <INPUT type="hidden" name="ResponseURL" value="<?php echo config('config.api_host') ?>payment/ipay88response">
	    <INPUT type="hidden" name="BackendURL" value="<?php echo config('config.api_host') ?>ipay88beckendresponse">
	</FORM>
</BODY>
</HTML>
<script type="text/javascript">
$(document).ready(function(){
	setSignature();
});

function setSignature(){
	var full = "<?php echo config('config.api_host') ?>payment/ipay88sha1";
	console.log(full);
	$.ajax({
		method: "POST",
		url: full,
		data: { merchantcode: $("input[name='MerchantCode']").val(), refno : $("input[name='RefNo']").val(), amount: $("input[name='Amount']").val(), currency: $("input[name='Currency']").val() }
	}).done(function( result ) {
		console.log(result);
	    console.log( result.signature );
	    $("input[name='Signature']").val(result.signature);
	    $("#ipay88frm").submit();
	});
}
</script>