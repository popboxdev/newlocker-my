<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Express extends Model
{
    protected $table = 'tb_newlocker_express';

    public function expressChecking(Request $req, $imported)
    {
     
        // MERCHANT RETURN
        $merchant = new MerchantReturn;
        $merchant_list = $merchant->getMerchant();

        // cek di tabel express
        $sql = "select * from tb_newlocker_express where deleteFlag = '0' and customerStoreNumber = '".$imported."' and expressType = 'CUSTOMER_STORE' and status = 'IMPORTED'";
        $exp = DB::select($sql);
        
        if (count($exp) != 0 ) {
            // ambil data logistic company
            $logisticsCompany_id = $exp[0]->logisticsCompany_id;

            if(empty($logisticsCompany_id)){
                $logisticsCompany_id  = '161e5ed1140f11e5bdbd0242ac110001';
            }

            $sqlc = "select * from tb_newlocker_company where id_company='".$logisticsCompany_id."'";
            $rc = DB::select($sqlc);  

            if (!empty($rc)) {
                // ambil data parent company
                $sqlp = "select * from tb_newlocker_company where id_company='".$rc[0]->id_parent."'";
                $rp = DB::select($sqlp);
            } else {
                $rp = "";
            }
            
            if (!empty($rp) ) {
                // ambil data parent company level grand
                $sqlk = "select * from tb_newlocker_company where id_company='".$rp[0]->id_parent."'";
                $rk = DB::select($sqlk);
            } else {    
                $rk = "";
            }

            if (!empty($rk)) {
                $grandcompany =  array('contactPhoneNumber' => [], 'deleteFlag' => 0, 'name' => $rk[0]->company_name, 'contactEmail' => [], 'id' => $rk[0]->id_company, 'level' => $rk[0]->level, 'companyType' => $rk[0]->company_type );
            } else {
                $grandcompany = "";
            }

            if (!empty($rp)  ) {
                $parentcompany = array('contactPhoneNumber' => [], 'deleteFlag' => 0, 'name' => $rp[0]->company_name, 'parentCompany' => $grandcompany, 'contactEmail' => [], 'id' => $rp[0]->id_company, 'level' => $rp[0]->level, 'companyType' => $rp[0]->company_type );
            } else {
                $parentcompany = "";

            }
            $logistic = array('contactPhoneNumber' => [], 'deleteFlag' => 0, 'name' => $rc[0]->company_name, 'parentCompany' => $parentcompany , 'contactEmail' => [], 'id' => $rc[0]->id_company, 'level'=> $rc[0]->level, 'companyType' => $rc[0]->company_type );

            if ($exp[0]->groupName == 'POPDEPOSIT') {
                $parcelType = 'POPSAFE';
            } elseif (in_array($exp[0]->groupName, $merchant_list)) {
                $parcelType = 'ONDEMAND';
            } else {
                $parcelType = $exp[0]->groupName;
            }

            $res =  ['endAddress' => $exp[0]->endAddress , 'createTime' => $exp[0]->importTime, 'additionalPayment' => [] , 'expressType' => $exp[0]->expressType, 'status' => $exp[0]->status,  'recipientUserPhoneNumber' => $exp[0]->recipientUserPhoneNumber, 'logisticsCompany' => $logistic, 'customerStoreNumber' => $exp[0]->customerStoreNumber, 'id' => $exp[0]->id, 'barcode' => ['id' => $exp[0]->barcode_id] , 'items' => [], 'includedNumbers' => 0, 'chargeType' => 'NOT_CHARGE', 'recipientName' => $exp[0]->recipientName , 'takeUserPhoneNumber' => $exp[0]->takeUserPhoneNumber, 'parcelType' => $parcelType, 'groupName' => $exp[0]->groupName];

            if(!empty($exp[0]->designationSize)){
                $res['designationSize'] = $exp[0]->designationSize;
            }

        } else {
            $res =  ['statusCode' => 404 , 'errorMessage' => 'express not found'];
        }

        return $res;
    }

    public function getExpress($expres_id)
    {
        $data = Express::where('id', $expres_id)->first();
        return $data;
    }

    public function getExpressForSmsFailed(Type $var = null)
    {
        $data = Express::where('id', $expres_id)->where('status', 'IN_STORE')->first();
        return $data;
    }

    public function resetDeletedFlag($id_locker, $deleted_flag)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 404;
        $response->message = "Data Not Found";
        $data = Express::find($id_locker);
        
        if (!$data) {
            return $response;
        }

        try {
            $data->deleteFlag = $deleted_flag;
            $data->save();

            $response->isSuccess = true;
            $response->message = "Data have been changed";
            $response->data = $data;
            $response->code = 200;
            return $response;
        } catch (\Exception $e) {
            $response->code = 500;
            $response->message = $e->getMessage();
            return $response;
        }
    }

    public function getPopulateData($locker_id, $date_start, $date_end)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 404;
        $response->message = "Data Not Found";
        $data = [];

        if (!empty($date_start) && !empty($date_end)) {
            $data = Express::where('box_id', $locker_id)
                        ->where('deleteFlag', '0')
                        ->where(function($query) use ($date_start, $date_end) {
                            $query->whereBetween('storeTime', [$date_start, $date_end]);
                            $query->orWhereBetween('takeTime', [$date_start, $date_end]);
                        })
                        ->get();
            
            if (!$data->count()) {
                return $response;
            } else {
                $response->isSuccess = true;
                $response->data = $data;
                $response->code = 200;
                $response->message = "Data Locker $locker_id";
                return $response;
            }
        } else {
            return $response;
        }
    }

    public function extendMultiple($request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 404;
        $response->message = "Data Not Found";
        $data = [];

        $parcels = $request->input('parcels');
        $overdue_date = $request->input('overdue_date');
        $list = Express::select('id as id_parcel', 'tb_newlocker_express.*')->whereIn('id', $parcels)->get();
        
        if ($list->count()) {
            try {
                $list_data = [];
                DB::beginTransaction();
                foreach ($list as $key => $value) {
                    if ($value->status != "IN_STORE") {
                        $list_data[] = [
                            'express_number' => $value->expressNumber,
                            'messages' => "Cannot Extend Because status parcels is ". $value->status
                        ];
                    } else {
                        $overdue = strtotime($overdue_date ."23:00:00") * 1000;
                        $lockerTask = new LockerTasks;
                        $createTask = $lockerTask->syncToLocker($value->id_parcel, $overdue);
                        $messages = "Create request extend is Success";
                        
                        if (!$createTask->isSuccess) {
                            $messages = $createTask->message;
                        }

                        $list_data[] = [
                            'express_number' => $value->expressNumber,
                            'messages' => $messages
                        ];
                    }
                }
                DB::commit();

                $response->isSuccess = true;
                $response->code = 200;
                $response->message = "Multiple Extend Parcels";
                $response->data = $list_data;
                return $response;
            } catch (\Exception $e) {
                DB::rollback();
                $response->message = $e->getMessage();
                return $response;
            }
        }
    }

    public function rollbackInserts($request) 
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 404;
        $response->message = "Data Not Found";
        $data = [];

        try {
            //code...
            $id = $request->input('id');
            $locker_id = $request->input('locker_id');
            $store_user = $request->input('store_user');
            $barcode = $request->input('barcode');
            $phone = $request->input('phone');
            $locker_number = $request->input('locker_number');
            $locker_size = $request->input('locker_size');
            $storetime = $request->input('storetime');
            $taketime = $request->input('taketime');
            $overduetime = $request->input('overduetime');
            $status = $request->input('status');
            $validatecode = $request->input('validatecode');
            $groupname = $request->input('groupname');
            $operator_id = $request->input('operator_id');
            
            //  Get Mouth id & type by number
            $mouthModel = new Mouth;
            $getMouth = $mouthModel->getMouthByNumber($locker_id, $locker_number);
            
            if (!$getMouth->isSuccess) {
                $response->message = $getMouth->message;
                return $response;
            } else {
                $mouth = $getMouth->data;
            }
    
            //  Get user drop
            $lockerUserModel = new LockerUser;
            $getLockerUser = $lockerUserModel->getUserByDisplayname($store_user);
            
            if (!$getLockerUser->isSuccess) {
                $response->message = $getLockerUser->message;
                return $response;
            } else {
                $lockerUser = $getLockerUser->data;
            }
    
            // Checking data
            $check_is_exist = Express::find($id);
            if ($check_is_exist) {
                $response->code = 409;
                $response->message = "Data is Exist";
                return $response;
            }

            if ($groupname == 'POPSEND' || $groupname == 'TAPTOPICK' || $groupname == 'VCLEANSHOE' || $groupname == 'OMAISU' || $groupname == 'LABALABA' || $groupname == 'LOGON') {
                $express_type = 'CUSTOMER_STORE';
            } else if ($groupname == 'ZALORA' || $groupname == 'MATAHARIMALL' || $groupname == 'BLIBLI' || $groupname == 'LAZADA' || $groupname == 'JAVAMIFI' || $groupname == 'POPBOX-TEST' || $groupname == 'LOGON' || $groupname == 'ORIFLAME' || $groupname == 'RAYSPEED' || $groupname == 'FARMAKU') {
                $express_type = 'CUSTOMER_REJECT';
            } else {
                $express_type = 'COURIER_STORE';
            }
            $taketime = ( strtotime($taketime) * 1000 ) ;
            $rollback_insert = new Express;
            $rollback_insert->id = $id;
            $rollback_insert->expressType = $express_type;
            if ( $express_type == 'CUSTOMER_STORE' ) {
                $rollback_insert->customerStoreNumber = $barcode;
            } else {
                $rollback_insert->expressNumber = $barcode;
            }
            $rollback_insert->takeUserPhoneNumber = $phone;
            $rollback_insert->status = $status;
            $rollback_insert->groupName = $groupname;
            $rollback_insert->overdueTime = ( strtotime($overduetime) * 1000 );
            $rollback_insert->storeTime = ( strtotime($storetime) * 1000 );
            $rollback_insert->takeTime = (  $taketime < 0 ? null : $taketime );
            $rollback_insert->lastModifiedTime = ( time() * 1000 );
            $rollback_insert->syncFlag = 1;
            $rollback_insert->deleteFlag = 0;
            $rollback_insert->validateCode = $validatecode;
            $rollback_insert->version = 0;
            $rollback_insert->box_id = $locker_id;
            $rollback_insert->mouth_id = $mouth->id_mouth;
            $rollback_insert->logisticsCompany_id = $lockerUser->id_company;
            $rollback_insert->operator_id = ( !empty($operator_id) ? $operator_id : null );
            $rollback_insert->storeUser_id = $lockerUser->id_user;
            $rollback_insert->chargeType = 'NOT_CHARGE';
            $rollback_insert->save();

            $response->isSuccess = true;
            $response->message = "Data have been inserted to locker database.";
            $response->code = 200;
            return $response;
        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            return $response;
        }
        
    }

    public static function checkingMid($mid)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 404;
        $response->message = "MID is Unknown!";
        
        $data = DB::table('tb_newlocker_countries')->where('id', $mid)->first();

        if ( empty($data) ) return $response;

        $response->isSuccess = true;
        $response->code = 202;
        $response->country = strtolower($data->country);
        $response->message = "MID For $data->country";

        return $response;
    }

    public function voidCanceled($id_parcel, $void_info) 
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 404;
        $response->message = "Data Not Found";
        $data = [];

        try {
            DB::beginTransaction();
            $express = Express::find($id_parcel);
            if ( !$express ) {
                return $response;
            } else {

                if ( $express->status != 'CANCELED' ) {
                    $response->message = "Void action only for data have been Canceled";
                    $response->code = 403;
                    return $response;
                }

                // Get Locker Name
                $boxModel = new Box;
                $locker_name = $boxModel->getBoxName($express->box_id);

                // Get Locker Number 
                $mouthModel = new Mouth;
                $locker_number = $mouthModel->getLockerNumber($express->mouth_id);

                // Get Locker Size 
                $mouthTypeModel = new MouthType;
                $locker_size = $mouthTypeModel->getLockerNamebyId($locker_number['type']);
                
                $expressHistory = new ExpressHistory;
                $expressHistory->id_express = $id_parcel;
                $expressHistory->box_id = $express->box_id;
                $expressHistory->locker_name = $locker_name;
                $expressHistory->express_number = !empty($express->expressNumber) ? $express->expressNumber : $express->customerStoreNumber;
                $expressHistory->req_locker_size = $locker_size;
                $expressHistory->get_locker_number = $locker_number['number'];
                $expressHistory->status = $express->status;
                $expressHistory->stored_user_id = $express->storeUser_id;
                $expressHistory->reason = $express->reason;
                $expressHistory->remarks = json_encode($void_info);
                $expressHistory->store_time = date('Y-m-d H:i:s', ( $express->storeTime / 1000 ));
                $expressHistory->save();

                $express->storeTime = null;
                $express->overdueTime = null;
                $express->validateCode = null;
                $express->mouth_id = null;
                $express->box_id = null;
                $express->reason = null;
                $express->storeUser_id = null;
                $express->status = !empty( $express->importTime ) ? 'IMPORTED' : null;
                $express->save();

                DB::commit();

                $data = [
                    'id' => $id_parcel,
                    'express_number' => $express->expressNumber,
                    'status' => $express->status,
                    'store_time' => $express->storeTime,
                    'overdue_time' => $express->overdueTime
                ];

                $response->isSuccess = true;
                $response->code = 202;
                $response->message = "Void for express number $express->expressNumber is Success!";
                $response->data = [$data];
                return $response;
            }
        } catch (\Exception $e) {
            DB::rollback();
            $response->message = $e->getMessage();
            $response->code = 500;
            return $response;
        }
    }

    public static function listAllParcels( $request, $country )
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 404;
        $response->message = "Data Not Found";
        $data = [];

        try {
            $filter = $request->input('filter');
            $page = $request->input('page');
            $mid = $request->header('mid');

            $length = ( $page['length'] == 0 ) ? 1 : $page['length'];
            if ($length > 100) {
                $length = 100;
            }
            $offset = ( $page['offset'] == 0 ) ? 0 : ( $page['offset'] * $length ) - 1;

            $tracking_number = $filter['tracking_number'];
            $parcel_id = $filter['parcel_id'];
            $parcel_type = $filter['parcel_type'];
            $status = $filter['status'];
            $courier_name = $filter['courier_name'];
            // $pin_code = $filter['pin_code'];
            $store_time = $filter['store_time'];
            $take_time = $filter['take_time'];
            $overdue_time = $filter['overdue_time'];
            $phone_number = $filter['phone_number'];
            
            $data = DB::table('tb_newlocker_express as express')
                ->select('express.*', 'express.id as id_express', 'box.name as locker_name', 'mouth.number as locker_number', 'mouth_type.name as locker_size', 'company.company_name as company_logistics', 'user.displayname as courier_user_drop', 'user_taken.displayname as courier_user_taken', 'company_courier.company_name as courier_company_drop', 'company_courier_taken.company_name as company_courier_taken', 'detail.*')
                ->leftJoin('tb_detail_express as detail', 'detail.parcel_id', '=', 'express.id')
                ->leftJoin('tb_newlocker_box as box', 'box.id', '=', 'express.box_id')
                ->leftJoin('tb_newlocker_mouth as mouth', 'mouth.id_mouth', '=', 'express.mouth_id')
                ->leftJoin('tb_newlocker_mouthtype as mouth_type', 'mouth_type.id_mouthtype', '=', 'mouth.mouthType_id')
                ->leftJoin('tb_newlocker_user as user', 'user.id_user', '=', 'express.storeUser_id')
                ->leftJoin('tb_newlocker_user as user_taken', 'user_taken.id_user', '=', 'express.staffTakenUser_id')
                ->leftJoin('tb_newlocker_company as company', 'company.id_company', '=', 'express.logisticsCompany_id')
                ->leftJoin('tb_newlocker_company as company_courier', 'company_courier.id_company', '=', 'user.id_company')
                ->leftJoin('tb_newlocker_company as company_courier_taken', 'company_courier_taken.id_company', '=', 'user_taken.id_company')
                ->when( $tracking_number , function($query) use ( $tracking_number ) {    
                    if ( !empty($tracking_number) ) {
                        return $query->where('expressNumber', $tracking_number );
                    }
                })->when( $parcel_id , function($query) use ( $parcel_id ) {    
                    if ( !empty($parcel_id) ) {
                        return $query->where('express.id', $parcel_id );
                    }
                })->when( $parcel_type , function($query) use ( $parcel_type ) {
                    
                    if ($parcel_type == 'LASTMILE' || $parcel_type == 'LAST MILE') {
                        $type = 'COURIER_STORE';
                    } else if ($parcel_type == 'POPSEND') {
                        $type = 'CUSTOMER_STORE';
                    } else {
                        $type = 'CUSTOMER_REJECT';
                    }

                    if ( !empty($parcel_type) ) {
                        return $query->where('expressType', $type );
                    }
                })->when( $status , function($query) use ( $status ) {    
                    if ( !empty($status) ) {
                        return $query->where('express.status', $status );
                    }
                })->when( $store_time , function($query) use ( $store_time, $country ) {    
                    if ( !empty($store_time) ) {

                        if ( count($store_time) < 2 ) {
                            $start = strtotime($store_time[0]) * 1000;
                            $end = strtotime($store_time[0]) * 1000;
                            if ( $country == "malaysia") {
                                $start = self::convertTimeMalaysia($store_time[0]);
                                $end = self::convertTimeMalaysia($store_time[0]);
                            }
                            
                            return $query->whereBetween('express.storeTime', [$start, $end] );

                        } else {
                            $start = strtotime($store_time[0]) * 1000;
                            $end = strtotime($store_time[1]) * 1000;
                            if ( $country == "malaysia") {
                                $start = self::convertTimeMalaysia($store_time[0]);
                                $end = self::convertTimeMalaysia($store_time[1]);
                            }
                            return $query->whereBetween('express.storeTime', [$start['timestamps'], $end['timestamps']] );
                        }                        
                    }
                })->when( $take_time , function($query) use ( $take_time ) {    
                    if ( !empty($take_time) ) {

                        if ( count($take_time) < 2 ) {
                            $start = strtotime($take_time[0]) * 1000;
                            $end = strtotime($take_time[0]) * 1000;
                            if ( $country == "malaysia") {
                                $start = self::convertTimeMalaysia($take_time[0]);
                                $end = self::convertTimeMalaysia($take_time[0]);
                            }
                            return $query->whereBetween('express.takeTime', [$start['timestamps'], $end['timestamps']] );

                        } else {
                            $start = strtotime($take_time[0]) * 1000;
                            $end = strtotime($take_time[1]) * 1000;
                            if ( $country == "malaysia") {
                                $start = self::convertTimeMalaysia($take_time[0]);
                                $end = self::convertTimeMalaysia($take_time[1]);
                            }
                            return $query->whereBetween('express.takeTime', [$start['timestamps'], $end['timestamps']] );
                        }
                        
                    }
                })->when( $overdue_time , function($query) use ( $overdue_time ) {    
                    if ( !empty($overdue_time) ) {

                        if ( count($overdue_time) < 2 ) {
                            $start = strtotime($overdue_time[0]) * 1000;
                            $end = strtotime($overdue_time[0]) * 1000;
                            if ( $country == "malaysia") {
                                $start = self::convertTimeMalaysia($overdue_time[0]);
                                $end = self::convertTimeMalaysia($overdue_time[0]);
                            }
                            return $query->whereBetween('express.overdueTime', [$start['timestamps'], $end['timestamps']] );

                        } else {
                            $start = strtotime($overdue_time[0]) * 1000;
                            $end = strtotime($overdue_time[1]) * 1000;
                            if ( $country == "malaysia") {
                                $start = self::convertTimeMalaysia($overdue_time[0]);
                                $end = self::convertTimeMalaysia($overdue_time[1]);
                            }
                            return $query->whereBetween('express.overdueTime', [$start['timestamps'], $end['timestamps']] );
                        }
                        
                    }
                })->when( $courier_name , function ($query) use ( $courier_name ) {
                    if ( !empty($courier_name) ) {
                        return $query->where('user.displayname', 'like', "%$courier_name%" );
                    }
                })->when( $mid , function ($query) use ( $mid ) {
                    if ( !empty($mid) ) {
                        return $query->where('express.operator_id', $mid );
                    }
                })->skip( $offset )->take( $length )->get();
            
            $list_data = [];
            foreach ($data as $key => $value) {

                if ( $value->expressType == 'COURIER_STORE' ) {
                    $type = 'LASTMILE';
                } else if ( $value->expressType == 'CUSTOMER_STORE' ) {
                    $type = 'POPSEND';
                } else if ( $value->expressType == 'CUSTOMER_REJECT' ) {
                    $type = 'RETURN';
                }

                $signature = null;
                if ( $value->status == "CUSTOMER_TAKEN" && $value->operator_id == '4028808357ad74f50157bc5681567b9e' ) {
                    $signature = "http://pr0x-my.popbox.asia/img/signature/".$value->expressNumber."-".$value->validateCode.".jpg";
                }

                if ($value->status != 'IMPORTED') {
                    if ( $value->status == 'IN_STORE') {
                        $startTime = Carbon::parse(date('Y-m-d H:i:s', $value->storeTime / 1000 ));
                        $finishTime = Carbon::parse(date('Y-m-d H:i:s', time()));
                        
                    } else {
                        $startTime = Carbon::parse(date('Y-m-d H:i:s', $value->storeTime / 1000 ));
                        $finishTime = Carbon::parse(date('Y-m-d H:i:s', $value->takeTime / 1000));
                    }
                }

                $totalDuration = $finishTime->diffForHumans($startTime);
                
                if ( $mid == '4028808357ad74f50157bc5681567b9e' ) {
                    
                    $storeTime = empty($value->storeTime) ? null : self::convertTimeMalaysia(date('Y-m-d H:i:s', ($value->storeTime / 1000)));
                    $takeTime = empty($value->takeTime) ? null : self::convertTimeMalaysia(date('Y-m-d H:i:s', (($value->takeTime / 1000))));
                    $overdueTime = empty($value->overdueTime) ? null : self::convertTimeMalaysia(date('Y-m-d H:i:s', (($value->overdueTime / 1000))));
                    
                    $list_data[] = [
                        'parcel_number' => !empty( $value->expressNumber ) ? $value->expressNumber : $value->customerStoreNumber,
                        'type' => $type,
                        'location' => $value->locker_name,
                        'locker_number' => $value->locker_number,
                        'locker_size' => $value->locker_size,
                        'status' => $value->status,
                        'customer_phone' => $value->takeUserPhoneNumber,
                        'pin_code' => $value->validateCode,
                        'store_time' => $storeTime['date'],
                        'take_time' => $takeTime['date'],
                        'overdue_time' => $overdueTime['date']
                    ];
                    
                } else {
                    $list_data[] = [
                        'parcel_number' => !empty( $value->expressNumber ) ? $value->expressNumber : $value->customerStoreNumber,
                        'type' => $type,
                        'location' => $value->locker_name,
                        'locker_number' => $value->locker_number,
                        'locker_size' => $value->locker_size,
                        'status' => $value->status,
                        'customer_phone' => $value->takeUserPhoneNumber,
                        'pin_code' => $value->validateCode,
                        'import_time' => empty($value->import_time) ? null : date('c', (($value->import_time / 1000))),
                        'store_time' => empty($value->storeTime) ? null : date('c', (($value->storeTime / 1000))),
                        'take_time' => empty($value->takeTime) ? null : date('c', (($value->takeTime / 1000))),
                        'overdue_time' => empty($value->overdueTime) ? null : date('c', (($value->overdueTime / 1000))),
                        'dwelling_time' => $totalDuration,
                        'parcel_belongs_company' => $value->company_logistics,
                        'courier_drop' => $value->courier_user_drop,
                        'company_belongs_courier_drop' => $value->courier_company_drop,
                        'courier_taken' => $value->courier_user_taken,
                        'company_belongs_courier_taken' => $value->company_courier_taken,
                        'receiver_name' => $value->receive_name,
                        'receiver_no_identification' => $value->no_identification,
                        'receiver_identification_type' => ( $value->is_passport == 1 ) ? "Passport" : "IC Number",
                        'signature' => $signature
                    ];
                }

            }
            $response->message = "Data parcels";
            $response->isSuccess = true;
            $response->code = 202;
            $response->data = $list_data;
            return $response;
        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            $response->data = [];
            return $response;
        }
    }

    public function box()
    {
        return $this->belongsTo('App\Models\Box', 'box_id', 'id');
    }
    
    public function checkingByExpressNumber ( $express_number ) 
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 404;
        $response->message = "Data Not Found";
        $data = [];

        $data = Express::where('expressNumber', $express_number)->first();
        if ( ! $data ) return $response;
        
        $data = [
            'order_number' => $data->expressNumber,
            'status' => $data->status,
            'stored_time' => ! empty( $data->storeTime ) ? date('Y-m-d H:i:s', ($data->storeTime / 1000)) : null,
            'overdue_time' => ! empty( $data->overdueTime ) ? date('Y-m-d H:i:s', ($data->overdueTime / 1000)) : null,
            'taken_time' => ! empty( $data->takTime ) ? date('Y-m-d H:i:s', ($data->takTime / 1000)) : null,
            'cancel_availability' => ( $data->status == 'IMPORTED' ) ? true : false
        ];
        
        $response->isSuccess = true;
        $response->message = "Data $express_number is found";
        $response->code = 202;
        $response->data = $data;
        return $response;
    }

    private static function convertTimeMalaysia($time_convert)
    {
        $time = new \DateTime($time_convert, new \DateTimeZone('Asia/Jakarta'));
        $time->setTimezone(new \DateTimeZone('Asia/Kuala_Lumpur'));
        
        $res = [
            'timestamps' => strtotime($time->format('Y-m-d H:i:s')) * 1000,
            'date' => $time->format('Y-m-d H:i:sP')
        ]; 
        
        return $res;
    }
}
