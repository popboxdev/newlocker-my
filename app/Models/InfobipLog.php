<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfobipLog extends Model
{
    protected $connection = 'popboxlog';
    protected $table = 'tb_sms_log_infobip';
}
