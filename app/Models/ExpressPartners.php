<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Helpers\WebCurl;
use App\Http\Helpers\DHLMalaysiaAPI;
use App\Http\Helpers\Helper;
use App\Http\Helpers\MeikartaAPI;
use App\Http\Helpers\DigiCondoAPI;
use App\Http\Helpers\SmsSender;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ExpressPartners extends Model
{
    protected $table = 'tb_express_partners';

    public function add($request, $status)
    {
        $id_express = $request->input('id');
        $logistics_company = $request->input('logisticCompany');
        $express_number = $request->input('expressNumber');

        DB::beginTransaction();
        try {
            $expressPartner = new ExpressPartners;
            $expressPartner->id_express = $id_express;
            $expressPartner->express_number = $express_number;
            $expressPartner->logistics_company = $logistics_company;
            $expressPartner->last_status = $status;
            $expressPartner->save();
            DB::commit();
            return $expressPartner;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function updateData($id_express, $status, $express_number=null, $column="expressNumber")
    {
        $data = ExpressPartners::where('id_express', $id_express)->first();
        if (!$data) {
            $dataExpress = Express::where('id', $id_express)->orderBy('storeTime', 'desc')->first();
            if ($dataExpress->expressNumber == $express_number) {
                $data = ExpressPartners::where('express_number', $dataExpress->expressNumber)->first();
                if ($data) {
                    $id_express = $data->id_express;
                } else {
                    $result = $this->send_callback($express_number, $column, $status);
                    if (is_array($result)) {
                        $send_inbound = $result['status'];
                        $param_callback = $result['callback_param'];
                        $save_to_log = json_encode($result);
                    } else {
                        $save_to_log = $send_inbound =  $result;
                    }

                    if ($send_inbound == "Accepted" || $send_inbound == "SUCCESS") {
                        Helper::LogPayment($express_number. ' - '.($save_to_log).' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
                        return $send_inbound;
                    } else {
                        Helper::LogPayment($express_number. ' - '.$save_to_log.' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
                        return $save_to_log;
                    }
                }
            }
        }
        
        if ($data) {
            if ($data->last_status == "OVERDUE") {
                if ($status == "COURIER_TAKEN" || $status == "OPERATOR_TAKEN" || $status == "CUSTOMER_TAKEN") {
                    $last_status = $status;
                } else {
                    $last_status = $data->last_status;
                }
            } else {
                $last_status = $status;
            }
            
            $data->last_status = $last_status;
            $data->save();
            if ($column == 'expressNumber') {
                $result = $this->send_callback($data->express_number, $column, $last_status); 
            } else {
                $result = $this->send_callback($data->id_express, $column, $last_status); 
            }
            $param_callback = "";
            
            if (is_array($result)) {
                $send_inbound = $result['status'];
                $param_callback = $result['callback_param'];
                $save_to_log = json_encode($result);
            } else {
                $save_to_log = $send_inbound =  $result;
            }
            
            if (isset($send_inbound)) {
                
                if ($send_inbound == "Accepted" || $send_inbound == "SUCCESS"){
                    $partners = ExpressPartners::where('id_express', $id_express)->first();
                    $partners->response_status = $param_callback;
                    if ($status == 'IN_STORE') {
                        $partners->is_inbound_sent = 1;
                        $partners->save();
                    } else if ($status == 'CUSTOMER_TAKEN' || $status == 'OPERATOR_TAKEN' || $status == 'COURIER_TAKEN') {
                        $partners->is_outbound_sent = 1;
                        $partners->save();
                    } else  {
                        $partners->is_overdue_sent = 1;
                        $partners->save();
                    }
                    Helper::LogPayment($partners->express_number. ' - '.($save_to_log).' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
                    return $send_inbound;
                } else {
                    Helper::LogPayment($data->express_number. ' - '.$save_to_log.' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
                    if (is_array($result)) {
                        if (array_key_exists('callback_param', $result)) {
                            $partners = ExpressPartners::where('id_express', $id_express)->first();
                            $partners->response_status = $param_callback;
                            $partners->save();
                            if (json_decode($result['callback_param'], true)) {
                                return $result['callback_param'];
                            }
                        } else {
                            return $save_to_log;
                        }
                    }
                    return $save_to_log;
                }
            } 
        } else {
            $data = ['message' => 'Data Not Found =>'.$id_express];
            Helper::LogPayment($id_express. ' - '.json_encode($data).' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
            return json_encode($data);
        }
    }

    public function insertData($id_express, $express_number, $logistics_company, $status)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errMsg = null;
        DB::beginTransaction();
        try {
            $id_meikarta = \Config::get('logisticcompanyid.meikarta');
            $id_posmy = \Config::get('logisticcompanyid.pos_malaysia');
            $id_dhl_malaysia = \Config::get('logisticcompanyid.dhl_malaysia');
            $id_digital_condo = \Config::get('logisticcompanyid.digital_condo');

            $expressPartner = new ExpressPartners;
            $expressPartner->id_express = $id_express;
            $expressPartner->express_number = $express_number;
            $expressPartner->logistics_company = $logistics_company;
            $expressPartner->last_status = $status;
            $expressPartner->save();
            DB::commit();

            if  (
                    $logistics_company == $id_meikarta
                    || $logistics_company == $id_posmy
                    || $logistics_company == $id_dhl_malaysia
                    || $logistics_company == $id_dhl_malaysia
                    || $logistics_company == $id_digital_condo
                ) {
                $result = $this->send_callback($id_express, 'tb_newlocker_express.id', $status);

                if (is_array($result)) {
                    $send_inbound = $result['status'];
                    $param_callback = $result['callback_param'];
                    $save_to_log = json_encode($result);

                    $partners = ExpressPartners::where('id_express', $id_express)->first();
                    $partners->response_status = $param_callback;
                    if ($send_inbound == "Accepted" || $send_inbound == "SUCCESS"){
                        if ($status == 'IN_STORE') {
                            $partners->is_inbound_sent = 1;
                            $partners->save();
                        } else if ($status == 'CUSTOMER_TAKEN' || $status == 'OPERATOR_TAKEN' || $status == 'COURIER_TAKEN') {
                            $partners->is_outbound_sent = 1;
                            $partners->save();
                        } else {
                            $partners->is_overdue_sent = 1;
                            $partners->save();
                        }
                    } else {
                        $partners->save();
                    }
                } else {
                    $save_to_log = $send_inbound =  $result;
                }

                if ($send_inbound == "SUCCESS") {
                    Helper::LogPayment($express_number. ' - '.($save_to_log).' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
                    return $send_inbound;
                } else {
                    Helper::LogPayment($express_number. ' - '.$save_to_log.' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
                    return false;
                }
            }

            $response->isSuccess = true;
            return $response;
        } catch (\Exception $e) {
            $response->errMsg = $e->getMessage();
            return $response;
        }
    }

    public function send_callback($order_number, $column,$status=null)
    {
        
        $id_lazada = \Config::get('logisticcompanyid.lazada_express');
        $id_posmy = \Config::get('logisticcompanyid.pos_malaysia');
        $id_popex = \Config::get('logisticcompanyid.pop_express');
        $id_lazada_id = \Config::get('logisticcompanyid.lazada_indonesia');
        $id_meikarta = \Config::get('logisticcompanyid.meikarta');
        $digital_condo = \Config::get('logisticcompanyid.digital_condo');
        $id_dhl_malaysia = \Config::get('logisticcompanyid.dhl_malaysia');
        
        $data_parcel = DB::table('tb_newlocker_express')
                                ->select('tb_newlocker_express.id as id_express', 'tb_newlocker_box.id as box_id', 'tb_newlocker_box.name as locker_name', 'tb_newlocker_express.*','tb_detail_express.*','tb_newlocker_box.*')
                                ->leftJoin('tb_newlocker_box', 'tb_newlocker_box.id', '=', 'tb_newlocker_express.box_id')
                                ->leftJoin('tb_detail_express', 'tb_detail_express.parcel_id', '=', 'tb_newlocker_express.id')
                                ->where($column, $order_number)
                                ->first();

        
        if ($data_parcel) {
            $url_tracking = "https://popbox.asia/tracking/" . $data_parcel->expressNumber;
            $output_file_name = $data_parcel->expressNumber."-".$data_parcel->validateCode.".jpg";
            $url_signature = "http://pr0x-my.popbox.asia".'/img/signature/'.$output_file_name; 
            
            // signature path on server
            $date_time = date('c', (time()));
            $data_parcel->status = !empty($status) ? $status : $data_parcel->status; 
            $company_id = !empty($data_parcel->company_venue_locker) ? $data_parcel->company_venue_locker :  $data_parcel->logisticsCompany_id;
            $company = $this->set_company($company_id);
            $user_drop = $this->getUsername($data_parcel->storeUser_id);
            if (!$user_drop->isSuccess) {
                $username_drop = "UNDEFINED";
            } else {
                $username_drop = $user_drop->data->username;
            }
            if ($company) {
                if ( $company->id_company == $digital_condo ) {
                    if (preg_match('/[@]/', $data_parcel->name)) {
                        $locker_name = $data_parcel->name;
                    } else {
                        $locker_name = 'PopBox @ '. $data_parcel->name;
                    }
                    $company = $this->set_company($data_parcel->logisticsCompany_id);
                    $company_name = "";
                    if ( isset($company) ) {
                        $company_name = $company->company_name;
                    }
                    $handler = $this->getOperator($data_parcel->storeUser_id);
                    if ( $data_parcel->status == "IN_STORE" ) {
                        $locker_status = 100;
                    } elseif( $data_parcel->status == "CUSTOMER_TAKEN" ) {
                        $locker_status = 200;
                    } elseif( $data_parcel->status == "OVERDUE" ) {
                        $locker_status = 300;
                    } elseif( $data_parcel->status == "COURIER_TAKEN" || $data_parcel->status == "OPERATOR_TAKEN" ) {
                        $locker_status = 400;
                    } else {
                        $locker_status = 000;
                    }
                    $take_drop_time = ($data_parcel->status == "IN_STORE") ? ($data_parcel->storeTime/1000) : ($data_parcel->storeTime/1000);
                    $store_take_time = Helper::convertTime("Asia/Jakarta", "Asia/Kuala_Lumpur", date("Y-m-d H:i:s", $take_drop_time), "", "full_times");
                    $overdue_time = Helper::convertTime("Asia/Jakarta", "Asia/Kuala_Lumpur", date("Y-m-d H:i:s", ($data_parcel->overdueTime/1000)), "", "full_times");
                    
                    $params = [
                        "PopBoxSubscriptionCode" => ""
                        ,"UserMobileNumber"  =>  $data_parcel->takeUserPhoneNumber
                        ,"PBRequestStatusId" => $locker_status
                        ,"UnitName" => $locker_name
                        ,"PBRequestReferenceNo" => $data_parcel->expressNumber
                        ,"PBRequestDate" => date("m/d/Y", strtotime($store_take_time["converted_date"]))
                        ,"PBRequestTime" => $store_take_time["converted_hours_mins"]
                        ,"PopBoxDueDate" => date("m/d/Y", strtotime($overdue_time["converted_date"]))
                        ,"DeliveryPersonName" => $handler
                        ,"DeliveryComapnyName" => $company_name
                    ];
                    $push = DigiCondoAPI::pushNotifications($params, $data_parcel->box_id);
                    
                    if ( !$push->isSuccess ) {
                        $res = [
                            'callback_param' => $push->message,
                            'status' => 'FAILED'
                        ];
                    } else {
                        $res = [
                            'callback_param' => $push->message,
                            'status' => 'SUCCESS'
                        ];
                        
                    }
                    
                    return $res;
                } elseif ($company->id_company == $id_lazada || $company->id_company == $id_lazada_id) {
                    $token_lazadamy = env('LEL_TOKEN');
                    $url_lazadamy_prod = env('LEL_URL_CALLBACK') . $token_lazadamy; //production
                    $url_lazadid_prod = env('LEL_ID_URL_CALLBACK') . $token_lazadamy; //production
                    $url_lazadamy_dev = env('LEL_WEBHOOK_TEST'); //testing webhook

                    if (env('APP_ENV') == 'production') {
                        if ($company->id_company == $id_lazada_id) {
                            $url_callback = $url_lazadid_prod;
                        } else {
                            $url_callback = $url_lazadamy_prod;
                        }
                    } else {
                        $url_callback = $url_lazadamy_dev;
                    }

                    if (preg_match('/[@]/', $data_parcel->name)) {
                        $locker_name = $data_parcel->name;
                    } else {
                        $locker_name = 'PopBox @ '. $data_parcel->name;
                    }
                    $reason_code = "";
                    if ($data_parcel->status == "COURIER_TAKEN" || $data_parcel->status == "CUSTOMER_TAKEN") {
                        $reason_code = "00";
                    } else if ($data_parcel->status == "OVERDUE") {
                        $reason_code = "21";
                    } else if ($data_parcel->status == "OPERATOR_TAKEN") {
                        $reason_code = "02";
                    } else if ($data_parcel->status == "IN_STORE") {
                        $reason_code = "01";
                    }

                    if ($data_parcel->status == "OPERATOR_TAKEN" || $data_parcel->status == "COURIER_TAKEN") {
                        $status_parcel = 'COURIER_TAKEN';
                    } else {
                        $status_parcel = $data_parcel->status;
                    }

                    $param_update = [
                        "order_number" => $data_parcel->expressNumber, 
                        "tracking_number" => $data_parcel->expressNumber, 
                        "status" => $status_parcel,
                        "date_time" => $date_time,
                        "locker_location" => $locker_name, 
                        "locker_geolocation" => "", 
                        "reason_code" => $reason_code, 
                        "receiver_name" => (!empty($data_parcel->receive_name)) ? $data_parcel->receive_name : $data_parcel->takeUserPhoneNumber,
                        "receiver_id" => (!empty($data_parcel->no_identification)) ? $data_parcel->no_identification : "", 
                        "receiver_signature" => ($company->id_company == $id_lazada) ? $url_signature : "", 
                        "tracking_url" => $url_tracking
                    ];
                    
                    $curlHelper = new WebCurl;
                    $curl = $curlHelper->post($url_callback, $param_update);
                    
                    DB::table('tb_newlocker_generallog')->insert(
                        ['api_url' =>  $url_callback, 
                        'api_send_data' => json_encode($param_update),
                        'api_response' => json_encode($curl),
                        'response_date' => date("Y-m-d H:i:s")]); 
                    if ($curl == "" or $curl == "Accepted") {
                        $res = [
                            'callback_param' => json_encode($curl),
                            'status' => 'SUCCESS'
                        ];
                        return $res;
                    } else {
                        return $curl;
                    }
                } else if ($company->id_company == $id_posmy) { // POS MALAYSIA
                    $lockerBankId = $this->getLockerBankID($data_parcel->box_id);
                    if ($lockerBankId->isSuccess || $data_parcel->status == 'COURIER_TAKEN') {  
                        // if ($data_parcel->status == "CUSTOMER_TAKEN" || $data_parcel->status == "IN_STORE" || $data_parcel->status == "OPERATOR_TAKEN" || $data_parcel->status == 'COURIER_TAKEN') {
                            $header_token = env('POSMALAYSIA_TOKEN');
                            if ($data_parcel->status == "CUSTOMER_TAKEN") {
                                $url_callback = env('POSMALAYSIA_URL_DELIVERY'); 
                                $reason_code = "01";
                                $param_update = [
                                    "courierID" => $username_drop,
                                    "LockerBankID" => $lockerBankId->data->kiosk_id,
                                    "date" => date('dmY'),
                                    "time" => date('His'),
                                    "consignmentNo" => $data_parcel->expressNumber,
                                    "reasonCode" => $reason_code,
                                    "recipientName" => $data_parcel->receive_name,
                                    "recipientIC" => $data_parcel->no_identification,
                                    "recipientLocation" => "07",
                                    "damageCode" => "-",
                                    "authorizedName" => "-",
                                    "comment" => "PopBox",
                                    "alternativeAddress" => "-",
                                    "paymentType" => "-",
                                    "modeOfPayment" => "-",
                                    "totalPayment" => "-",
                                    "chequeNo" => "-",
                                    "bankCode" => "-",
                                    "dropCode" => "N",
                                    "lokasiDrop" => "-"
                                ];
                            } elseif ($data_parcel->status == "COURIER_TAKEN") {
                                $content = json_decode(SmsSender::smsContent('EXPRESS_OVERDUE_COURIER_TAKE_POSMY', 'MY'));
                                $url_callback = env('POSMALAYSIA_URL_DELIVERY'); 

                                $boxModel = new Box;
                                $box_name = $boxModel->getBoxName($data_parcel->box_id);
                                
                                $content_ = str_replace('{datePickup}', date('d/m/y H:i', $data_parcel->takeTime / 1000), $content->content);
                                
                                $content__ = str_replace('{orderNumber}', $data_parcel->expressNumber, $content_);
                                $message = str_replace('{box_name}', $box_name, $content__);
                                if (substr($data_parcel->takeUserPhoneNumber, 0, 2) == "01"){
                                    $res = [];
                                    $resp = [];
                                    $check_data_sms = DB::table('tb_newlocker_smslog')->where('express_id', $data_parcel->id_express)->get();
                                    if ( count( $check_data_sms ) <= 2 ) {
                                        $sms_provider = env('SMS_PROVIDER', 'ISENTRIC');

                                        if ( $sms_provider == "INFOBIP") {
                                            $resp = SmsSender::sendFromInfoBip( $data_parcel->takeUserPhoneNumber, $message, $data_parcel->id_express );
                                            $statusMsg = $resp;
                                            
                                        } else {
                                            $send_sms = SmsSender::sendFromIsentric($data_parcel->takeUserPhoneNumber, $message);
                                            $resp = json_decode($send_sms->getContent());
                                            $statusMsg = "SUCCESS";
                                            if (!$resp->data->messages[0]->status && $resp->data->messages[0]->status !=0) {
                                                DB::table('tb_newlocker_smslog')
                                                    ->insert([
                                                        'express_id' => $data_parcel->id_express,
                                                        'phone_number' => $data_parcel->takeUserPhoneNumber,
                                                        'sms_content' => '[Auto] '.$message,
                                                        'sms_status' => 'FAILED',
                                                        'sent_on' => date("Y-m-d H:i:s"),
                                                        'original_response' => json_encode($resp)
                                                    ]);  

                                                $res = [
                                                    'callback_param' => json_encode($resp),
                                                    'status' => 'FAILED'
                                                ];
                                                return $res;
                                            }
                                        }
                                        
        
                                        DB::table('tb_newlocker_smslog')
                                        ->insert([
                                            'express_id' => $data_parcel->id_express,
                                            'phone_number' => $data_parcel->takeUserPhoneNumber,
                                            'sms_content' => '[Auto] '.$message,
                                            'sms_status' => $statusMsg,
                                            'sent_on' => date("Y-m-d H:i:s"),
                                            'original_response' => json_encode($resp)
                                        ]);  
                                    }
                                }
                                
                                $res = [
                                    'callback_param' => $message,
                                    'status' => 'SUCCESS'
                                ];
                        
                                return $res;
                            } else {
                                $url_callback = env('POSMALAYSIA_URL_STATUS'); 
                                $header_token = env('POSMALAYSIA_TOKEN_STATUS');
                                $status_code  = (env('APP_ENV') == "development") ? 1 : 50;
                                if (env('APP_ENV') == "development") {
                                    $status_code = 1;
                                } else {
                                    if ($data_parcel->status == "OVERDUE") {
                                        $status_code = 53;
                                    } else {
                                        $status_code = 50;
                                    }
                                }
                                $param_update = [
                                    "courierID" => $username_drop,
                                    "LockerBankID" => $lockerBankId->data->kiosk_id,
                                    "date" => date('dmY'),
                                    "time" => date('His'),
                                    "consignmentNo" => $data_parcel->expressNumber,
                                    "statusCode" => $status_code,
                                    "comment" => "-",
                                    "receiverPostCode" => "-",
                                    "senderAccNo" => "-"
                                ];
                            }
                            
                            $header[] = 'Content-Type: application/json';
                            $header[] = 'X-User-Key: '. $header_token;
                            
                            $curlHelper = new WebCurl($header);
                            $curl = $curlHelper->post($url_callback, json_encode($param_update));
        
                            $result = !empty($curl) ? json_decode($curl) : $curl;
                            DB::table('tb_newlocker_generallog')->insert(
                                ['api_url' =>  $url_callback, 
                                'api_send_data' => json_encode($param_update),
                                'api_response' => json_encode($curl),
                                'response_date' => date("Y-m-d H:i:s")]); 
                            
                            try {
                                if ($result) {
                                    if ($result->StatusCode == "SS01" || $result->StatusCode == "SD01" || $result->Message == "Success" ) {
                                        $res = [
                                            'callback_param' => json_encode($result),
                                            'status' => 'SUCCESS'
                                        ];
                                        return $res;
                                    } else {
                                        return $curl;
                                    }  
                                } else {
                                    return $curl;
                                }
                            } catch (\Exception $e) {
                                return $e->getMessage();
                            }
                        // } else {
                        //     $res = [
                        //         'callback_param' => "",
                        //         'status' => 'Only For CUSTOMER_TAKEN'
                        //     ];
                        //     return $res;
                        // }
                    } else {
                        $res = [
                            'callback_param' => "",
                            'status' => 'Kiosk ID not Found'
                        ];
                        return $res;
                    }

                } elseif ($company->id_company == $id_popex) { // FOR CALLBACK POPEXPRESS
                    $handler = "";
                    if ($data_parcel->status == "OPERATOR_TAKEN" || $data_parcel->status == "COURIER_TAKEN") {
                        $handler = $this->getOperator($data_parcel->staffTakenUser_id);
                    } elseif ($data_parcel->expressType == "COURIER_STORE" && $data_parcel->status == "IN_STORE") {
                        $handler = $this->getOperator($data_parcel->storeUser_id);
                    } else {
                        $handler = $data_parcel->recipientName;
                    }
                    $url_callback = env('POPEXPRESS_CALLBACK_URL');
                    $token_popex  = env('POPEXPRESS_TOKEN');
                    $date_store = Carbon::parse(date('Y-m-d H:i:s', $data_parcel->storeTime / 1000));
                    $now = Carbon::now();
                    $aging = $date_store->diffInDays($now);
                    
                    $param_update = [
                        "api_key" => $token_popex,
                        "awb" => $data_parcel->expressNumber,
                        "status" => $data_parcel->status,
                        "update_date" => date('Y-m-d H:i:s'),
                        "aging" => $aging,
                        "locker_id" => $data_parcel->box_id,
                        "handler_name" => $handler
                    ];
                    
                    $curlHelper = new WebCurl(); 
                    $curl = json_decode($curlHelper->post($url_callback, $param_update));
                    
                    DB::table('tb_newlocker_generallog')->insert(
                        ['api_url' =>  $url_callback, 
                        'api_send_data' => json_encode($param_update),
                        'api_response' => json_encode($curl),
                        'response_date' => date("Y-m-d H:i:s")]); 
                    
                    if ($curl->response->code == 200) {
                        $res = [
                            'callback_param' => json_encode($param_update),
                            'status' => 'SUCCESS'
                        ];
                        return $res;
                    } else {
                        $res = [
                            'callback_param' => json_encode($curl),
                            'status' => 'FAILED'
                        ];
                        
                        return $res;
                    }
                    
                } elseif ($company->id_company == $id_meikarta) {
                    $action = 'create';                    
                    $handler_store = "";
                    $handler_take = "";

                    if ($data_parcel->status == "OPERATOR_TAKEN" || $data_parcel->status == "COURIER_TAKEN") {
                        $handler_take = $this->getOperator($data_parcel->staffTakenUser_id);
                        $handler_store = $this->getOperator($data_parcel->storeUser_id);
                    } elseif ($data_parcel->expressType == "COURIER_STORE" && $data_parcel->status == "IN_STORE") {
                        $handler_store = $this->getOperator($data_parcel->storeUser_id);
                    } else {
                        $handler_take = $data_parcel->takeUserPhoneNumber;
                        $handler_store = $this->getOperator($data_parcel->storeUser_id);
                    }

                    if ($data_parcel->status != 'IN_STORE') {
                        $action = 'update';
                    }

                    $param_update = [
                        'parcel_id' => $data_parcel->id_express,
                        'order_number' => $data_parcel->expressNumber,
                        'location' => $data_parcel->locker_name,
                        'store_time' => date('Y-m-d H:i:s', $data_parcel->storeTime / 1000),
                        'overdue_time' => date('Y-m-d H:i:s', $data_parcel->overdueTime / 1000),
                        'pin' => $data_parcel->validateCode,
                        'drop_person' => $handler_store,
                        'take_person' => $handler_take,
                        'status' => $data_parcel->status,
                        'phone' => "",
                        'email' => $data_parcel->email_customer,
                        'project_id' => 6
                    ];
                    
                    $checking = ExpressPartners::where('id_express', $data_parcel->id_express)
                            ->where('last_status', $data_parcel->status)
                            ->where('last_status')
                            ->first();

                    if ($checking) {
                        $meikartaAPI = new MeikartaAPI;
                        $res = json_decode($meikartaAPI->sendtoMeikarta(json_encode($param_update), $action));
                    } else {
                        $res = [
                            'callback_param' => "",
                            'status' => "ALREADY_PUSH"
                        ];
                    }

                    
                    if ($res) {
                        if (isset($res->status)) {
                            $res = [
                                'callback_param' => json_encode($res),
                                'status' => $res->status
                            ];
                            
                            return $res;
                        } else {
                            $res = [
                                'callback_param' => json_encode($param_update),
                                'status' => 'SUCCESS'
                            ];
                            return $res;
                        }
                    } else {
                        return $res;
                    }
                } elseif ($company->id_company == $id_dhl_malaysia) {
                    $handler = "";
                    if ($data_parcel->status == "OPERATOR_TAKEN" || $data_parcel->status == "COURIER_TAKEN") {
                        $handler = $this->getOperator($data_parcel->staffTakenUser_id, "DHL");
                    } elseif ($data_parcel->expressType == "COURIER_STORE" && $data_parcel->status == "IN_STORE" || $data_parcel->status == "CUSTOMER_TAKEN") {
                        $handler = $this->getOperator($data_parcel->storeUser_id, "DHL");
                    }

                    // $dhlPush = DHLMalaysiaAPI::push($data_parcel, $handler);
                    // return $dhlPush;
                    return true;
                } else {
                    return "Company Not Found";
                }
            } else {
                return "Company Not Found";
            }
        } else {
            return "Data Not Found";
        }
    }

    public function getOperator($user_id, $company_type = null)
    {
        $user = DB::table('tb_newlocker_user')->where('id_user', $user_id)->first();
        if ($user) {
            if ( isset($company_type) ) {
                $data_user = $user->username;
            } else {
                $data_user = $user->displayname;
            }
            return $data_user;
        }

        return $user;
    }

    private function set_company($company_id)
    {
        $companyDb = DB::table('tb_newlocker_company')->where('id_company', $company_id)->first();
        if ($companyDb) {
            return $companyDb;
        }
        return false;
    }

    public function getLockerBankID($box_id)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = [];
        $data = MasterLockerBankIdPosMy::where('box_id', $box_id)->first();
        if (!$data) {
            return $response;
        } else {
            if ($data->kiosk_id == null) {
                return $response;
            }
            $response->isSuccess = true;
            $response->data = $data;
            return $response;
        }
    }

    public function getUsername($user_id)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errMsg = null;
        $response->data = [];
        $user = DB::table('tb_newlocker_user')->where('id_user', $user_id)->first();
        if (!$user) {
            $response->errMsg = "User Not Found";
            return $response;
        }

        $response->data = $user;
        $response->isSuccess = true;
        return $response;
    }

    public static function updateDoubleDataLzd($id_express, $express_number, $status)
    {
        try {
            $response = new \stdClass();
            $response->isSuccess = false;
            $response->code = 404;
            $response->message = "Data Not Found";
            $data = [];

            $check_data = ExpressPartners::where('id_express', $id_express)->first();
            
            if (!$check_data) {
                $check_by_express_number = ExpressPartners::where('express_number', $express_number)->first();

                if ($check_by_express_number) {
                    try {
                        $update = ExpressPartners::find($check_by_express_number->id);
                        $update->last_status = $status;
                        $update->save();
                    
                        if ($update->save()) {
                            $updateExpress = Express::find($update->id_express);
                            $updateExpress->status = $status;
                            
                            $getDataLockerNotImported = Express::where('expressNumber',     $express_number)->whereNull('importTime')->first();
                            if ($status == "CUSTOMER_TAKEN" || $status == "OPERATOR_TAKEN" || $status == "COURIER_TAKEN") {
                                $getDataLockerNotImported->deleteFlag = 1;
                                $getDataLockerNotImported->status = $status;
                                $getDataLockerNotImported->save();
                            }

                            $updateExpress->expressType = $getDataLockerNotImported->expressType;
                            $updateExpress->groupName = $getDataLockerNotImported->groupName;
                            $updateExpress->customerStoreNumber =$getDataLockerNotImported->customerStoreNumber;
                            $updateExpress->overdueTime = $getDataLockerNotImported->overdueTime;
                            $updateExpress->storeTime = $getDataLockerNotImported->storeTime;
                            $updateExpress->syncFlag = $getDataLockerNotImported->syncFlag;
                            $updateExpress->takeTime = $getDataLockerNotImported->takeTime;
                            $updateExpress->storeUserPhoneNumber = $getDataLockerNotImported->storeUserPhoneNumber;
                            $updateExpress->validateCode = $getDataLockerNotImported->validateCode;
                            $updateExpress->version = $getDataLockerNotImported->version;
                            $updateExpress->box_id = $getDataLockerNotImported->box_id;
                            $updateExpress->logisticsCompany_id =$getDataLockerNotImported->logisticsCompany_id;
                            $updateExpress->mouth_id = $getDataLockerNotImported->mouth_id;
                            $updateExpress->operator_id = $getDataLockerNotImported->operator_id;
                            $updateExpress->storeUser_id = $getDataLockerNotImported->storeUser_id;
                            $updateExpress->takeUser_id = $getDataLockerNotImported->takeUser_id;
                            $updateExpress->recipientName = $getDataLockerNotImported->recipientName;
                            $updateExpress->weight = $getDataLockerNotImported->weight;
                            $updateExpress->chargeType = $getDataLockerNotImported->chargeType;
                            $updateExpress->electronicCommerce_id =$getDataLockerNotImported->electronicCommerce_id;
                            $updateExpress->staffTakenUser_id =$getDataLockerNotImported->staffTakenUser_id;
                            $updateExpress->drop_by_btn_kirim =$getDataLockerNotImported->drop_by_btn_kirim;
                            $updateExpress->email_customer = $getDataLockerNotImported->email_customer;
                            $updateExpress->lastModifiedTime = time() * 1000;
                            $updateExpress->status = $status;
                            $updateExpress->save();

                            
                        }

                        $response->isSuccess = true;
                        $response->code = 200;
                        $response->message = "updated data success";
                        return $response;
                    } catch (\Exception $e) {
                        $response->message = $e->getMessage();
                        $response->code = 500;
                        return $response;
                    }
                }
            } else {
                $response->message = "data is exist";
                return $response;
            }
        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            $response->code = 500;
            return $response;
        }
    }

    public function checkStatusUpdate($id_express, $express_number, $status) 
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 200;
        $response->message = "No need to push";
        $data = [];
        
        try {
                $check_data = ExpressPartners::when($status, function($query) use ($status) {
                if ($status == "CUSTOMER_TAKEN") {
                    return $query->orWhere(function ($query_lzd) {
                        $query_lzd->where('last_status', 'CUSTOMER_TAKEN')
                            ->where('is_outbound_sent', null);
                    });
                } else if ($status == "IN_STORE") {
                    return $query->orWhere(function ($query_lzd) {
                        $query_lzd->where('last_status', 'IN_STORE')
                            ->where('is_inbound_sent', null);
                    });
                } else if ($status == "COURIER_TAKEN") {
                    return $query->orWhere(function ($query_lzd) {
                        $query_lzd->where('last_status', 'COURIER_TAKEN')
                            ->where('is_overdue_sent', null);
                    });
                } else if ($status == "OPERATOR_TAKEN") {
                    return $query->orWhere(function ($query_lzd) {
                        $query_lzd->where('last_status', 'OPERATOR_TAKEN')
                            ->where('is_outbound_sent', null);
                    });
                } else {
                    return $query->orWhere(function ($query_lzd) {
                        $query_lzd->where('last_status', 'IMPORTED')
                            ->where('is_inbound_sent', null);
                    });
                }
                
            })
            ->where( function ($query) use ($express_number, $id_express) {
                $query->orWhere('express_number', $express_number);
                $query->orWhere('id_express', $id_express);
            })
            ->first();

            if ( isset($check_data) ) {
                $response->isSuccess = true;
                $response->message = "Need to push";
                return $response;
            } else {
                $partners = ExpressPartners::where('id_express', $id_express)->first();

                if ( $partners->status != $status ) {
                    $response->isSuccess = true;
                    $response->message = "Need to push";
                    return $response;
                } else {
                    $response->isSuccess = false;
                    return $response;
                }
            }

        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            $response->code = 500;
            return $response;
        }
    }
}
