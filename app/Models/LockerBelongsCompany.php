<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class LockerBelongsCompany extends Model
{
    protected $table = 'tb_locker_belongs_company';

    public function store($data)
    {
        $response = new \stdClass;
        $response->isSuccess = false;
        $response->message = null;
        $response->code = 500;

        $country = $data->input('country');
        $company = $data->input('id_company');
        $locker = $data->input('locker');

        $set_delete_company = $this->resetLockerbyCompanyId($company);
        
        if (!$set_delete_company->isSuccess) {
            $response->message = "Cannot update locker Company because ".$set_delete_company->message;
            return $response;
        }
        
        if (count($locker) != 0) {
            try {
                DB::beginTransaction();
                foreach ($locker as $key => $value) {
                    $LockerCompanyModel = new LockerBelongsCompany;
                    $LockerCompanyModel->box_id = $value['locker_id'];
                    $LockerCompanyModel->locker_name = $value['locker_name'];
                    $LockerCompanyModel->building_types = $value['bulding_type'];
                    $LockerCompanyModel->country = $country;
                    $LockerCompanyModel->parent_id_company = $company;
                    $LockerCompanyModel->save();
                }
                DB::commit();
                $response->isSuccess = true;
                $response->code = 200;
                $response->message = "Success add locker belongs to company";
                return $response;
            } catch (\Exception $e) {
                DB::rollback();
                $response->message = $e->getMessage();
                return $response;
            }
        }
    }

    private function resetLockerbyCompanyId($id_company)
    {
        $response = new \stdClass;
        $response->isSuccess = false;
        $response->message = null;
        $response->code = 500;

        $lockerCompany = LockerBelongsCompany::where('parent_id_company', $id_company)
                        ->whereNull('is_deleted')
                        ->orWhere('is_deleted', 0)
                        ->get();
        
        if (count($lockerCompany) != 0) {
            try {
                DB::beginTransaction();
                foreach ($lockerCompany as $key => $value) {
                    $value->is_deleted = 1;
                    $value->save();
                }
                DB::commit();
                $response->isSuccess = true;
                $response->code = 200;
                $response->message = "Set delete for company success";
                return $response;
            } catch (\Exception $e) {
                DB::rollback();
                $response->message = $e->getMessage();
                return $response;   
            }
        }

        $response->isSuccess = true;
        $response->message = "Data Null";
        return $response;
    }

    public function listLocker($locker_id, $filter)
    {
        $response = new \stdClass;
        $response->isSuccess = false;
        $response->message = 'Data Not Found';
        $response->code = 404;

        $building_type = null;
        if (!empty($filter['building_types'])) {
            $building_type = $filter['building_types'];
        }

        $dataLocker = LockerBelongsCompany::where('parent_id_company', $locker_id)
        ->where(function($query) {
            $query->where('is_deleted','!=', '1');
            $query->orWhereNull('is_deleted');
        })->when($building_type, function($query) use ($building_type) {
            if (!empty($building_type)) {
                return $query->where('building_types', $building_type);
            }
        })->get();

        $locker_list = [];
        $building_types = [];
        if (count($dataLocker) == 0) {
            return $response;
        } else {
            foreach ($dataLocker as $key => $value) {
                $locker_list[] = [
                    'locker_id' => $value->box_id,
                    'locker_name' => $value->locker_name,
                ];
                $building = $this->getBuilding($value->building_types);
                $building_types[] = $building;
            }
            
            $response->isSuccess = true;
            $response->code = 200;
            $response->message = "Data list locker by company";
            $response->data = [
                'list_locker' => $locker_list,
                'list_building_types' => $building_types,
            ];
            return $response;
        }
    }

    private function getBuilding($id_building)
    {
        $data = DB::table('tb_newlocker_buildingtypes')->where('id_building', $id_building)->first();
        
        $building = [];
        if ($data) {
            $building = [$data->id_building => $data->building_type];
            
        }
        return $building;
    }

    public static function getParentCompany($locker_id)
    {
        try {
            $response = new \stdClass;
            $response->isSuccess = false;
            $response->message = 'Data Not Found';
            $response->code = 404;

            $dataLocker = LockerBelongsCompany::where('box_id', $locker_id)->whereNull('is_deleted')->first();
            if ( isset($dataLocker) ) {
                $response->isSuccess = true;
                $response->code = 200;
                $response->message = "Data locker";
                $response->data = (object)[
                    'locker_id' => $dataLocker->box_id,
                    'parent_company' => $dataLocker->parent_id_company
                ];
            }

            return $response;
        
        } catch (\Exception $e) {
            return $response;
        }
    }
}
