<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhatsappLogMalaysia extends Model
{
    protected $connection = "popboxlog";
    protected $table = "tb_wa_log_malaysia";

    public static function checking( $parcel_id )
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 409;
        $response->message = "Data already exists";
        $data = [];

        try {
            $waLog = WhatsappLogMalaysia::where('parcel_id', $parcel_id)->first();
            if ( $waLog ) {
                return $response;
            }
            $response->isSuccess = true;
            $response->code = 202;
            $response->message = "Data not exists";
            return $response;
        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            return $response;
        }
    }
}
