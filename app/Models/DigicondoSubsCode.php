<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DigicondoSubsCode extends Model
{
    protected $table = 'tb_digicondo_subscode';
}
