<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParcelCompanyNotification extends Model
{
    protected $table = 'tb_parcel_company_notification';

    public static function getCompanyNoSendSMSNotification()
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->code = 404;
        $response->message = "Data Not Found";

        try {
            $data = ParcelCompanyNotification::where('send_sms_notification', 0)
                ->where('is_active', 1)
                ->get();

            if ( empty($data) ) return $response;

            $list = [];
            foreach ($data as $key => $value) {
                $list[] = $value->logistics_company_id;
            }

            $response->data = $list;
            $response->isSuccess = true;
            $response->code = 202;
            return $response;
            
        } catch (\Exception $e) {
            $response->code = 500;
            $response->message = $e->getMessage();
            return $response;
        }
    }
}
