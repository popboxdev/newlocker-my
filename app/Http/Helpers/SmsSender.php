<?php namespace App\Http\Helpers;

use App\Http\Helpers\WebCurl;
use App\Models\InfobipLog;
use Illuminate\Support\Facades\DB;

class SmsSender {
	
	var $to, $message;
	
	function __construct($to, $message) {
		$this->to = $to;
		$this->message = $message;
	}
	
	public function send() {
		$curl = new WebCurl(['Content-Type: application/json']);
		$url = 'http://smsdev.popbox.asia/sms/send';
		$params = json_encode(['to' => $this->to, 'message' => $this->message, 'token' => '0weWRasJL234wdf1URfwWxxXse304']);
		$response = $curl->post($url, $params);
		
		return strpos($response, 'Status=1') != false;
	}

	public static function sendFromIsentric ($to, $message){
        $response = ['response' => ['code' => 500, 'message' => 'ERROR'], 'data' => []];
        if(!empty($to) && !empty($message)) {
            if(substr($to, 0, 1) == '+') {
                $to = substr($to, 1);
            } else if(substr($to, 0, 1) == '0') {
                $to = '60'.substr($to, 1);
            } else if(substr($to, 0, 1) == '1') {
                $to = '60'.$to;
            }
            $message = urlencode($message);
            $mtid = "707".time().rand(111, 999);
            $accountName = 'popboxsunway'; //This is live account, please don't change..! [Wahyudi 09-09-17]
            // $server_ip = '203.223.130.118';
            $server_ip = '203.223.130.115';
            $runfile = 'http://'.$server_ip.'/ExtMTPush/extmtpush?shortcode=39398&custid='.$accountName.'&rmsisdn='.$to.'&smsisdn=62003&mtid='.$mtid.'&mtprice=000&productCode=&productType=4&keyword=&dataEncoding=0&dataStr='.$message.'&dataUrl=&dnRep=0&groupTag=10';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $runfile);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            
            $content = curl_exec ($ch);
            $ret = strpos($content, "returnCode = ");
            $start = $ret + 13;
            $retcode = substr($content, $start, 1);
            curl_close ($ch); 
            $response = [
                'response' => [
                    'code' => 200, 
                    'message' => 'OK'
                ], 
                'raw' => $content,
                'data' => [
                    'message-count' => '1', 
                    'messages' => 
                        [
                            [
                                'to' => $to, 
                                'message_id' => $mtid,
                                'status' => $retcode, 
                                'remaining_balance' => '0', 
                                'message_price' => '0', 
                                'network'=>'0'
                            ]
                        ]
                    ]
			];

            DB::table('tb_newlocker_generallog')
                ->insert([
                    'api_url' => 'http://'.$server_ip.'/ExtMTPush/extmtpush?shortcode=39398',
                    'api_send_data' => $runfile,
                    'api_response' => $content,
                    'response_date' => date("Y-m-d H:i:s")
                ]);
		}      
        return response()->json($response);       
	}

	public static function sendFromInfoBip($phone, $message, $expressId)
    {
        if ( ! empty($phone) && ! empty($message) ) {
            
            if(substr($phone, 0, 1) == '+') {
                $phone = substr($phone, 1);
            } else if(substr($phone, 0, 2) == '62') {
                $phone = '0'.substr($phone, 2);
            } else if(substr($phone, 0, 1) == '8') {
                $phone = '0'.$phone;
            } else if(substr($phone, 0, 2) == '01') { //Can also handle Malaysia Phone Number
                $phone = '6'.$phone;
            } else if (substr($phone, 0, 1) == '1') {
                $phone = '60'.$phone;
            }
            
            $body = [
                'messages' => [
                    [
                        "from" => "POPBOX-ASIA",
                        "destinations" => [
                            [
                                "to" => $phone
                            ]
                        ],
                        "text" => $message,
                        "intermediateReport" => true,
                        "notifyUrl" => env('PR0X_URL')."/sms/infobip/callback",
                        "notifyContentType" => "Content-type: application/json",
                        "callbackData" => $expressId,
                        "validityPeriod" => 720
                    ]
                ],
                "tracking" => [
                    "track" => "SMS",
                    "type" => "ONE_TIME_PIN"
                ]
            ];
            $url = env('URL_INFOBIP');
            $token = env('URL_INFOBIP_TOKEN');

            $headers[] = 'Content-type: application/json';
            $headers[] = 'Authorization: Basic '. $token;

            $curl = new WebCurl( $headers );
            $response = $curl->post( $url, json_encode( $body ));

			$result = json_decode( $response );
            $infobip = new InfobipLog();
            $infobip->id_express = $expressId;

            if ( property_exists( $result, 'requestError') ) {
                $infobip->status = 'ERROR';
                $infobip->callback = $response;
                $response = 'ERROR';
            } else {
                $infobip->message_id = $result->messages[0]->messageId;
                $infobip->group_name_status = $result->messages[0]->status->name;
                $infobip->status = $result->messages[0]->status->groupName;
                $infobip->description = $result->messages[0]->status->description;
                $response = $result->messages[0]->status->groupName;
            }

            $infobip->save();

            return $response;
        }
    }
	
}
