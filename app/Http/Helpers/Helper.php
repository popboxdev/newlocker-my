<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 17/01/2017
 * Time: 9:23
 */

namespace App\Http\Helpers;

use Carbon\Carbon;

class Helper
{
    /**
     * generate random string
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10) {
        $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function queueEmail($from='',$to='',$fileView){

    }

    public static function buildResponse($code=400,$message='Failed Response',$data=array()){
        $response = new \stdClass();
        $response->code = $code;
        $response->message = $message;

        $result = new \stdClass();
        $result->response = $response;
        if (!is_array($data)) {
            $result->data = [$data];
        } else {
            $response->total = count($data);
            $result->data = $data;
        }
        return response()->json($result);
    }

    /**
     * @param $message
     * @param string $folderName
     * @param string $filename
     * @param null $sessionId
     */
    public static function LogPayment($message,$folderName='',$filename='payment',$sessionId=null){
        $path = storage_path().'/logs/'.$folderName;
        if (!is_dir($path)){
            mkdir($path);
        }

        $message = "$sessionId > $message\n";
        $f = fopen($path.'/'.$filename.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $message");
        fclose($f);
    }

    public static function respObject($data, $message="",$isSuccess=false)
    {
        $response = new \stdClass;
        $response->isSuccess = false;
        $response->data = [];

        if ($isSuccess) {
            $response->isSuccess = $isSuccess;
            $response->message = $message;
            $response->data = (object)$data;
            return $response;
        } else {
            $response->message = $message;
            return $response;
        }
    }

    public static function validation($request, $required)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errMsg = null;
        $response->code = 500;

        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errMsg = $message;
            return $response;
        }

        $response->isSuccess = true;
        $response->code = 200;
        return $response;
    }
    
    public static function overdueAddWeekDays($date, $duration, $type="DAY")
    {
        if ($type == "DAY") {
            $time = "23:00:00";
            $add_date_weekdays = (strtotime ( $date."+".$duration." weekdays $time" )) * 1000;
        } else {
            $time = 60*60*$duration;
            $add_date_weekdays = (strtotime($date)+$time) * 1000;
            
        }
        return $add_date_weekdays;
    }

    public static function diffYears($date)
    {
        $dbDate = \Carbon\Carbon::parse($date);
        $diffYears = \Carbon\Carbon::now()->diffInYears($date);
        return $diffYears;
    }

    public static function getHoursTimestamps($hours_start=true)
    {
        $time = time();
        $date = date('Y-m-d', time());
        $hours = date('H', $time);
      
        if ($hours_start) {
            $hours -= 1;
        }
        $date_hours = strtotime($date." $hours:00:00") * 1000;
        
        return $date_hours;
    }
    
    public static function getListTimestampsByDate($date)
    {
        $list_time = [];
        $date_hours = [];
        for ($i=1; $i < 25; $i++) { 
            $hours = (strlen($i) <= 1) ? "0".$i : $i;
            $tmp_end_h = (int)($hours - 1);
            $start_hours = (strlen($tmp_end_h) <= 1) ? "0".$tmp_end_h : (string)$tmp_end_h;
            
            $date_hours[] = [
                'start_date' => strtotime($date." $start_hours:00:00") * 1000,
                'end_date' => strtotime($date." $hours:00:00") * 1000
            ];
        }
        return $date_hours;
    }

    public static function getAllDateBetweenDate($start_date, $end_date)
    {
        $startDate = new Carbon($start_date);
        $endDate = new Carbon($end_date);
        
        $all_dates = [];
        while ($startDate->lte($endDate)){
            $all_dates[] = $startDate->toDateString();
            $startDate->addDay();
        }
        
        return $all_dates;
        
    }

    public static function sendErrToTelegram($e) {
        $err_param = [
            "id" => time(),
            "client_id" => "001",
            "locker_name" => "SERVER-PROX-INDONESIA",
            "type" => "ERROR_HANDLER",
            "error_value" => "<ERRMSG> ".$e->getMessage()." <ERRLINE> ". $e->getLine() . "<ERRFILE> ". $e->getFile()
        ];
        
        $header[] = 'Content-Type: application/json';
        $curlHelper = new WebCurl($header);
        $curl = $curlHelper->post('http://komodo.popbox.asia/senderror', json_encode($err_param));
    }
    
    public static function convertTime($origin_timezone, $destination_timezone, $dates, $times, $convert_type = "partly_times")
    {
        $data = [];
        try {
            date_default_timezone_set($origin_timezone);
            if ( $convert_type == "full_times") {
                $date_convert = strtotime($dates);
            } else {
                $date_convert = strtotime($dates." ".$times.":00:00");

            }
            date_default_timezone_set($destination_timezone);
            $data = [
                "converted_date" => date("Y-m-d", $date_convert),
                "converted_hours" => date("H", $date_convert),
                "converted_hours_mins" => date("H:i", $date_convert),
                "converted_full" => Carbon::createFromFormat('Y-m-d H:i:s', date("Y-m-d H:i:s", $date_convert))->toIso8601ZuluString(),
            ];
            return $data;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        
    }
}