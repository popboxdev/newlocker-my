<?php
namespace App\Http\Helpers;


use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use App\Http\Helpers\Helper;
use Illuminate\Support\Facades\DB;
use App\Models\DigicondoSubsCode;

class DigiCondoAPI
{
    private static function push_request($endpoint, $params)
    {
        $headers[] = "Content-Type: application/json";
        $headers[] = "DigiCoAuth-Token: ".env('DC_AUTH_TOKEN');
        $headers[] = "DigiCoAuthKey: ".env('DC_AUTH_KEY');
        $headers[] = "DigiCentralUserId: ".env('DC_SENTRAL_USER');

        $url = env("DC_URL").$endpoint;
        $webCurl = new WebCurl($headers);
        $push = $webCurl->post($url, json_encode($params));
        return $push;
    }

    public static function validateUser($customer_phone, $box_id)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $subs_code = self::getSubsCode($box_id);
        if ( empty($subs_code) ) {
            return $response;
        }
        $params = [
            "UserMobileNumber" => $customer_phone,
            "PopBoxSubscriptionCode" => $subs_code
        ];
        $url = "/api/DGC_PopBox/ValidateUser";
        $push = self::push_request($url, $params);

        if ( ! $push ) {
            return $response;
        }

        $result = json_decode( $push );
        if ( $result->errorCode != 0 ) {
            return $response;
        }    
        
        $response->isSuccess = true;
        return $response;
    }

    public static function pushNotifications($params, $box_id)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->message = "";

        $subs_code = self::getSubsCode($box_id);
        if ( empty($subs_code) ) {
            return $response;
        }
        $params["PopBoxSubscriptionCode"] = $subs_code;
        $url = "/api/DGC_PopBox/SendNotification";
        $push = self::push_request($url, $params);
        if ( ! $push ) {
            $response->message = json_encode( $push );
            return $response;
        }

        $result = json_decode( $push );
        if ( $result->errorCode != 0 ) {
            $response->message = json_encode( $push );
            return $response;
        }    
        
        $response->isSuccess = true;
        $response->message = json_encode( $push );
        return $response;
    }
    
    public static function getSubsCode($box_id) 
    {
        $subs_code = null;
        $digiCondoModel = DigicondoSubsCode::where('box_id', $box_id)->first();
        if ( isset($digiCondoModel) ) {
            $subs_code = $digiCondoModel->subscription_code;
        }
        return $subs_code;
    }



}